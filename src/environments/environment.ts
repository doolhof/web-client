// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD2hHT_qHQe6hfbglnFnlbkq9dvqYZ9iOE',
    authDomain: 'mazetrix-c09ac.firebaseapp.com',
    databaseURL: 'https://mazetrix-c09ac.firebaseio.com/',
    projectId: 'mazetrix-c09ac',
    storageBucket: 'mazetrix-c09ac.appspot.com',
    messagingSenderId: '250908293848'
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
