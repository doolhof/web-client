export class Game {
  gameId: number;
  startDate: Date;
  time: Date;
  status: string;
  gameBoard: GameBoard;
  playerList: Player[];
  turnTime: number;
  originalGame: number;

  constructor(gameId: number,
              startDate: Date,
              time: Date,
              status: string,
              gameBoard: GameBoard,
              playerList: Player[],
              turnTime: number,
              originalGame: number) {
    this.gameId = gameId;
    this.startDate = startDate;
    this.time = time;
    this.status = status;
    this.gameBoard = gameBoard;
    this.playerList = playerList;
    this.turnTime = turnTime;
    this.originalGame = originalGame;
  }
}


export class Tile {
  tileId: number;
  position: number;
  mazeCard: MazeCard;
  moveable: boolean;

  constructor(tileId: number, position: number, mazeCard: MazeCard, moveable: boolean) {
    this.tileId = tileId;
    this.position = position;
    this.mazeCard = mazeCard;
    this.moveable = moveable;
  }
}

export class GameBoard {
  gameBoardId: number;
  tiles: Tile[];

  constructor(gameBoardId: number, tiles: Tile[]) {
    this.gameBoardId = gameBoardId;
    this.tiles = tiles;
  }
}

export class MazeCard {
  mazeCardId: number;
  orientation: number;
  northWall: boolean;
  eastWall: boolean;
  southWall: boolean;
  westWall: boolean;
  imageType: number;
  treasure: Treasure;


  constructor(mazeCardId: number, orientation: number, northWall: boolean,
              eastWall: boolean, southWall: boolean, westWall: boolean, imageType: number, treasure: Treasure) {
    this.mazeCardId = mazeCardId;
    this.orientation = orientation;
    this.northWall = northWall;
    this.eastWall = eastWall;
    this.southWall = southWall;
    this.westWall = westWall;
    this.imageType = imageType;
    this.treasure = treasure;
  }

}

export class Player {
  playerId: number;
  color: string;
  name: string;
  toFind: Treasure[];
  found: Treasure[];
  turns: Turn[];
  currentPosition: number;
  startPosition: number;
  turn: boolean;


  constructor(playerId: number, color: string, name: string, toFind: Treasure[],
              found: Treasure[], turns: Turn[], currentPosition: number, startPosition: number, turn: boolean) {
    this.playerId = playerId;
    this.color = color;
    this.name = name;
    this.toFind = toFind;
    this.found = found;
    this.turns = turns;
    this.currentPosition = currentPosition;
    this.startPosition = startPosition;
    this.turn = turn;
  }
}

export class Treasure {
  treasureId: number;
  name: string;

  constructor(treasureId: number, name: string) {
    this.treasureId = treasureId;
    this.name = name;
  }
}

export class Turn {
  turnId: number;
  turnDuration: number;
  turnPhase: string;
  turnDate: Date;
  playerMoves: number[];
  player: number;
  // waar tile ingeschoven mazecard id
  posMazeCardMove: number;
  // rotatie tile
  orientation: number;
  playerName: string;


  constructor(turnId: number, turnDuration: number, turnPhase: string, turnDate: Date,
              playerMoves: number[], player: number, posMazeCardMove: number, orientation: number, playerName: string) {
    this.turnId = turnId;
    this.turnDuration = turnDuration;
    this.turnPhase = turnPhase;
    this.turnDate = turnDate;
    this.playerMoves = playerMoves;
    this.player = player;
    this.posMazeCardMove = posMazeCardMove;
    this.orientation = orientation;
    this.playerName = playerName;
  }
}


/**
 * gameMessage for websSocket*/
export class GameSocketMessage {
  gameId: string;
  userId: string;

  constructor(gameId: string, userId: string) {
    this.gameId = gameId;
    this.userId = userId;
  }
}

/**
 * PlayerMessage for webSockets*/
export class PlayerSocketMessage {
  playerId: string;
  destination: string;

  constructor(playerId: string, destination: string) {
    this.playerId = playerId;
    this.destination = destination;
  }
}

/**
 * TileMessage for webSockets*/
export class TileSocketMessage {
  position: string;
  gameBoardId: string;

  constructor(position: string, gameBoardId: string) {
    this.position = position;
    this.gameBoardId = gameBoardId;
  }
}
