import {Injectable} from '@angular/core';
import {
  HttpErrorResponse,
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpSentEvent,
  HttpUserEvent
} from '@angular/common/http';
import {Router} from '@angular/router';
import {TokenStorage} from './token.storage';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {error} from 'util';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class Interceptor implements HttpInterceptor {

  constructor(private token: TokenStorage, private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    let authReq = req;
    if (this.token.getToken() != null) {
      authReq = req.clone({headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + this.token.getToken())});
    }
    if (!authReq.headers.has('Content-Type')) {
      authReq = authReq.clone({headers: authReq.headers.set('Content-Type', 'application/json')});
    }
    // setting the accept header
    authReq = authReq.clone({headers: authReq.headers.set('Accept', 'application/json')});
    return next.handle(authReq).pipe(catchError(err => {
      this.handleAuthError(err);
      return of(null);
    }) as any);
  }

  private handleAuthError(err: HttpErrorResponse): Observable<any> {
    // handle your auth error or rethrow
    if (err.status === 401) {
      // navigate /delete cookies or whatever
      this.router.navigate(['/login']);
    } else if (err.status === 500 || err.status === 404) {
     console.log('api call mislukt');
     this.router.navigate(['/error']);
    }
    throw error;
  }

}
