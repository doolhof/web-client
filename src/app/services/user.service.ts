import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {FbInfo, PasswordDto, Token, User} from '../../model/user/user';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }


  getUserByUserId(id: number): Observable<User> {
    return this.http.get<User>('http://localhost:9090/api/users/id/' + id.toString(), {headers: this.headers}).pipe(
      map(res => new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
        res.userStatistic, res.userSetting, res.notifications, res.fcmToken)));
  }

  getUserByEmail(email: string): Observable<User> {
    return this.http.get<User>('http://localhost:9090/api/users/email/' + email, {headers: this.headers}).pipe(
      map(res => new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
        res.userStatistic, res.userSetting, res.notifications, res.fcmToken)));
  }

  getUserByUsername(username: string): Observable<User> {
    return this.http.get<User>('http://localhost:9090/api/users/username/' + username, {headers: this.headers}).pipe(
      map(res => new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
        res.userStatistic, res.userSetting, res.notifications, res.fcmToken),
        catchError(err => {
          return of(null);
        })));
  }

  postLogin(username: string, password: string): Observable<Token> {
    return this.http.post<Token>('http://localhost:9090/api/users/signin?username=' + username + '&password=' + password
      , {headers: this.headers})
      .pipe(map(res => new Token(res.token, res.user)),
        catchError(err => {
          return of(null);
        }));
  }

  postSignUp(username: string, password: string, email: string): Observable<Token> {

    return this.http.post<Token>(`http://localhost:9090/api/users/sign-up/${username}/${password}/${email}` , {headers: this.headers})
      .pipe(map(res => new Token(res.token, res.user)),
        catchError(err => {
          return of(null);
        }));
  }

  postConfirm(token: string): Observable<Boolean> {
    console.log(token);
    return this.http.post<Boolean>('http://localhost:9090/api/users/confirm?token=' + token, {headers: this.headers});
  }

  updateUser(user: User): Observable<User> {
    return this.http.put<User>('http://localhost:9090/api/users/update', user, {headers: this.headers})
      .pipe(map(res => new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
        res.userStatistic, res.userSetting, res.notifications, res.fcmToken)),
        catchError(err => {
          return of(null);
        }));
  }

  updatePassword(passwordDto: PasswordDto): Observable<Boolean> {
    return this.http.put<Boolean>('http://localhost:9090/api/users/updatePassword', passwordDto, {headers: this.headers});
  }

  deleteAccount(userId: number): Observable<Boolean> {
    return this.http.delete<Boolean>('http://localhost:9090/api/users/deleteAccount/' + userId, {headers: this.headers});
  }

  createAccountWithFb(fbInfo: FbInfo): Observable<User> {
    console.log('in postsignup');
    return this.http.post<User>('http://localhost:9090/api/users/sign-up-fb', fbInfo, {headers: this.headers})
      .pipe(map(res => new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
        res.userStatistic, res.userSetting, res.notifications, res.fcmToken)),
        catchError(err => {
          return of(null);
        }));
  }

  updateFcmToken(user: User): Observable<User> {
    return this.http.post<User>('http://localhost:9090/api/users/updateFcmToken?token=' + user.fcmToken + '&id=' + user.userId,
      {headers: this.headers})
      .pipe(map(res => new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies,
        res.userStatistic, res.userSetting, res.notifications, res.fcmToken)),
        catchError(err => {
          return of(null);
        }));
  }

  isLoggedIn(): boolean {
    if (JSON.parse(window.localStorage.getItem('currentUser')) !== null) {
      return true;
    } else {
      return false;
    }
  }

  getLoggedInUser(): User {
    return JSON.parse(window.localStorage.getItem('currentUser'));
  }

  logout() {
    window.localStorage.removeItem('currentUser');
  }

}

