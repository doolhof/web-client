import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Turn} from '../../model/game/game';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TurnService {
  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }

  endTurn(turnDuration: number, turnPhase: string,
          posMazeCardMove: number, playerMoves: number[], player: number, game: number, orientation: number): Observable<Turn> {
    return this.http.post<Turn>(`http://localhost:9090/api/turn/endTurn/${turnDuration}/
    ${turnPhase}/${posMazeCardMove}/${playerMoves.toString()}/${player}/${orientation}`, {headers: this.headers});
  }

  getTurns(gameId: number): Observable<Turn[]> {
    return this.http.get<Turn[]>(`http://localhost:9090/api/turn/getTurns/${gameId}`, {headers: this.headers}).pipe(map(res =>
      res.map(x => new Turn(x.turnId, x.turnDuration, x.turnPhase,
        x.turnDate, x.playerMoves, x.player, x.posMazeCardMove, x.orientation, x.playerName))));
  }
}
