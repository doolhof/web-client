import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Notificat, User} from '../../model/user/user';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import {not} from 'rxjs/internal-compatibility';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  headers = new HttpHeaders();


  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }

  getAllNotifications(id: number): Observable<Notificat[]> {
    return this.http.get<Notificat[]>(`http://localhost:9090/api/notification/${id}`, {headers: this.headers}).pipe(
    map(res => res.map(x =>
      new Notificat(x.notificationId, x.notifDesc, x.user, x.notificationType, x.webUrl, x.mobileUrl, x.closed))));
  }

  putNotificationToClose(id: number) {
    return this.http.post(`http://localhost:9090/api/notification/close/${id}`, {headers: this.headers})
  .pipe(
      map(res => console.log(res))
    );
  }
}
