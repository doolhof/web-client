import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../../model/user/user';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FriendService {
  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }

  getFriendsByUser(userId: number): Observable<User[]> {
    return this.http.get<User[]>('http://localhost:9090/api/friends/' + userId).pipe(
      map(res => res.map(x => new User(x.userId, x.username, x.friends, x.email, x.encryptedPassword, x.lobbies, x.userStatistic, x.userSetting, x.notifications, x.fcmToken)))
    );
  }

  addFriend(userId: number, friendId: number): Observable<User> {
    return this.http.put<User>('http://localhost:9090/api/friends/add/' + userId.toString() + '/' + friendId.toString(),
      null, {headers: this.headers}).pipe(
      map(res => res = new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies, res.userStatistic, res.userSetting, res.notifications, res.fcmToken))
    );
  }

  removeFriendByUser(userId: number, friendId: number): Observable<User> {
    return this.http.put<User>('http://localhost:9090/api/friends/remove/' + userId.toString() + '/' + friendId.toString(),
      null, {headers: this.headers}).pipe(
      map(res =>
        res = new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies, res.userStatistic, res.userSetting, res.notifications, res.fcmToken))
    );
  }
}
