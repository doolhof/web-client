import { TestBed } from '@angular/core/testing';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from '../app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import { GameService } from './game.service';
import {AppModule} from '../app.module';

describe('GameService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [AppModule]}));

  it('should be created', () => {
    const service: GameService = TestBed.get(GameService);
    expect(service).toBeTruthy();
  });
});
