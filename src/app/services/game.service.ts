import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Game, GameBoard, MazeCard} from '../../model/game/game';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class GameService {

  headers = new HttpHeaders();
  // http://localhost:9090/
  // https://doolhof-backend.herokuapp.com/
  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }

  // GET
  getGameboard(gameboardId: number): Observable<GameBoard> {
    return this.http.get<GameBoard>('http://localhost:9090/api/gameboard/' + gameboardId.toString(), {headers: this.headers}).pipe(
      map(res => new GameBoard(res.gameBoardId, res.tiles))
    );
  }

  getNewGame(lobbyId: number): Observable<Game> {
    return this.http.get<Game>('http://localhost:9090/api/game/startGame/' + lobbyId.toString(), {headers: this.headers})
      .pipe(map(game => new Game(game.gameId, game.startDate, game.time, game.status, game.gameBoard,
        game.playerList, game.turnTime, game.originalGame)));
  }

  // POST
  postRemainingMazeCard(mazecard: MazeCard): void {
    this.http
      .post('http://localhost:9090/api/mazecard/updateRemaining', mazecard, {headers: this.headers})
      .toPromise()
      .then(res => res.toString());
  }

  // POST
  postMove(position: Number, gameboardId: Number): Observable<GameBoard> {
    return this.http.post<GameBoard>(`http://localhost:9090/api/tile/moveTile?gameboardId=${gameboardId}`, position)
      .pipe(map(res => new GameBoard(res.gameBoardId, res.tiles)));
  }

  getGame(id: number): Observable<Game> {
    return this.http.get<Game>('http://localhost:9090/api/game/' + id)
      .pipe(map(game => new Game(game.gameId, game.startDate, game.time, game.status, game.gameBoard,
        game.playerList, game.turnTime, game.originalGame)));
  }

  getGamesUser(username: String): Observable<Game[]> {
    console.log('in getgamesuser');
    return this.http.get<Game[]>('http://localhost:9090/api/game/loadAllGames/' + username)
      .pipe(map(res => res.map(game => new Game(game.gameId, game.startDate, game.time, game.status, game.gameBoard,
        game.playerList, game.turnTime, game.originalGame))));
  }

  getReplayGame(gameId: number): Observable<Game> {
    console.log('CALL ME MAYBE');
    return this.http.get<Game>(`http://localhost:9090/api/game/replayGame/${gameId}`)
      .pipe(map(game => new Game(game.gameId, game.startDate, game.time, game.status, game.gameBoard, game.playerList,
        game.turnTime, game.originalGame)));
  }

  replayStopped(gameId: number): void {
    this.http.post(`http://localhost:9090/api/game/stopReplayGame/${gameId}`, {headers: this.headers});
  }

}
