import {Injectable} from '@angular/core';
import {Lobby, User} from '../../model/user/user';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Game} from '../../model/game/game';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LobbyService {
  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }

  postCreateLobby(userId: number, secondsTurn: number, amountPlayer: number): Observable<Lobby> {
    return this.http.post<Lobby>(`http://localhost:9090/api/lobby/createLobby/${userId}/${amountPlayer}/${secondsTurn}`,
      {headers: this.headers})
      .pipe(map(res => new Lobby(res.lobbyId, res.listUsers, res.gameSettings, res.host, res.game)));
  }

  getLobbyById(id: number): Observable<Lobby> {
    return this.http.get<Lobby>(` http://localhost:9090/api/lobby/${id}`)
      .pipe(map(res => new Lobby(res.lobbyId, res.listUsers, res.gameSettings, res.host, res.game)));

  }

  getLobbiesUser(username: string): Observable<Lobby[]> {
    console.log('in getLobbies');
    return this.http.get<Lobby[]>(`http://localhost:9090/api/lobby/loadLobbies/${username}`, {headers: this.headers})
      .pipe(map(res => res.map(x =>
          new Lobby(x.lobbyId, x.listUsers, x.gameSettings, x.host, x.game)),
        catchError(err => {
          return of(null);
        })));
  }

  sendInvitation(users: string[], lobbyId: number)  {
    console.log(users.toString());
    return this.http.post(`http://localhost:9090/api/lobby/invite/${lobbyId}?userIds=${users}`, {headers: this.headers});
  }

  getCommunity(userId: number, min: number, max: number): Observable<User[]> {
    return this.http.get<User[]>(`http://localhost:9090/api/users/community/${userId}/${min}/${max}`, {headers: this.headers}).pipe(
      map(x => x.map(res =>
        new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies, res.userStatistic, res.userSetting, res.notifications, res.fcmToken))));
  }

  searchUsersByUsername(username: string, userId: number): Observable<User[]> {
    return this.http.get<User[]>(`http://localhost:9090/api/users/usernames/${username}/${userId}`, {headers: this.headers}).pipe(
    map(x => x.map(res =>
      new User(res.userId, res.username, res.friends, res.email, res.encryptedPassword, res.lobbies, res.userStatistic, res.userSetting, res.notifications, res.fcmToken))));
  }
}
