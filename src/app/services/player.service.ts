import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GameBoard, Player, Tile} from '../../model/game/game';
import {map} from 'rxjs/operators';
import {User} from '../../model/user/user';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }

  movePlayer(playerId: number, destination: number): Observable<Tile[]> {
    return this.http.post<Tile[]>(`http://localhost:9090/api/player/move?playerId=${playerId}&destination=${destination}`
      , {headers: this.headers}).pipe(map(res => res.map(x =>
      new Tile(x.tileId, x.position, x.mazeCard, x.moveable))));
  }

  movePlayerWithTile(playerId: number, destination: number): Observable<Player[]> {
    return this.http.post<Player[]>(`http://localhost:9090/api/player/movePlayerTile?playerId=${playerId}&destination=${destination}`
      , {headers: this.headers}).pipe(map(res => res.map(x =>
      new Player(x.playerId, x.color, x.name, x.toFind, x.found, x.turns, x.currentPosition, x.startPosition, x.turn))));
  }

  getAllPlayers(gameId: number): Observable<Player[]> {
    return this.http.get<Player[]>(`http://localhost:9090/api/player/game/${gameId}`, {headers: this.headers}).pipe(
      map(res => res.map(x =>
        new Player(x.playerId, x.color, x.name, x.toFind, x.found, x.turns, x.currentPosition, x.startPosition, x.turn))));
  }

  // searchTreasure(playerId: number): Observable<Player> {
  //   return this.http.post<Player>(`http://localhost:9090/api/player/treasureSearch?playerId=${playerId}`,
  //     {headers: this.headers}).pipe(
  //     map(player => new Player(player.playerId, player.color, player.name, player.toFind, player.found,
  //       player.turns, player.currentPosition, player.startPosition)));
  // }

//   checkWinGame(playerId: number): Observable<boolean> {
//     return this.http.post<Player>(`http://localhost:9090/api/player/checkWinGame?playerId=${playerId}`,
//       {headers: this.headers}).pipe(
//       map(res => Boolean(res)));
// =======
//         new Player(x.playerId, x.color, x.name, x.toFind, x.found, x.turns, x.currentPosition, x.startPosition, x.turn))));
// >>>>>>> master
//   }

}
