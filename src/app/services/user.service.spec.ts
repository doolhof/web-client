import {TestBed} from '@angular/core/testing';

import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from '../app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';

import {UserService} from './user.service';
import {AppModule} from '../app.module';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('UserService', () => {
  beforeEach(() => TestBed.configureTestingModule({imports: [AppModule]}));

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });
});

describe('UserService post confirm', () => {
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService],
    });
    httpMock = TestBed.get(HttpTestingController);

    const req = httpMock.expectOne(`http://localhost:9090/api/users/confirm?token=DFsdefds12dd`, 'call to api');
    expect(req.request.method).toBe('POST');
  });
});
