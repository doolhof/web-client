import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../../../model/user/user';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-friend-statistics',
  templateUrl: './friend-statistics.component.html',
  styleUrls: ['./friend-statistics.component.scss']
})
export class FriendStatisticsComponent implements OnInit {
  userId: number;
  user: User;
  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router) {
    this.route.paramMap.subscribe(params => {
      this.userId = +params.get('id');
      this.userService.getUserByUserId(this.userId).subscribe(res => {
        this.user = res;
      });
    });
  }

  ngOnInit() {
  }

  onBackFriends() {
    this.router.navigateByUrl('/friends');

  }
  onBackMain() {
    this.router.navigateByUrl('/mainmenu');

  }
}
