import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FriendStatisticsComponent } from './friend-statistics.component';

describe('FriendStatisticsComponent', () => {
  let component: FriendStatisticsComponent;
  let fixture: ComponentFixture<FriendStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FriendStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
