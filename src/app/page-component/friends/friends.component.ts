import { Component, OnInit } from '@angular/core';
import {User} from '../../../model/user/user';
import {UserService} from '../../services/user.service';
import {FriendService} from '../../services/friend.service';
import {Router} from '@angular/router';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {
  user: User;
  friends: User[];
  selectedFriend: User;
  showAddMenu: boolean;

  constructor(private userService: UserService, private friendService: FriendService, private router: Router, private notifierService: NotifierService) {
    this.user = userService.getLoggedInUser();
    this.friendService.getFriendsByUser(this.user.userId).subscribe(res => this.friends = res);
    // this.friends = [new User(500, 'Buddy', 'jfsdklfjms', 'buddy', null, null, null),
    // new User(501, 'Mate', 'ffffff', 'mate', null, null, null),
    // new User(502, 'BFF', 'kljkjljl', 'bff', null, null, null),
    // new User(503, 'Freddy', 'fffssss', 'freddy', null, null, null)];
  }

  ngOnInit() {
    this.showAddMenu = false;
  }

  onSelect(friend: User) {
      this.selectedFriend = friend;
  }

  onUnselect() {
    this.selectedFriend = null;
  }

  onRemove(friend: User) {
    this.friendService.removeFriendByUser(this.user.userId, friend.userId).subscribe(res => {
      this.user = res;
      this.friends = res.friends;
    });
    this.notifierService.notify('default', friend.username + ' removed from friends list.');
  }

  onStats(friend: User) {
    console.log(friend);
    this.router.navigate(['/user/' + friend.userId]);
  }

  onBack() {
    this.router.navigate(['/']);
  }

  onUpdateUser() {
    this.userService.getUserByUserId(this.user.userId).subscribe(res => this.user = res);
    this.friendService.getFriendsByUser(this.user.userId).subscribe(res => {
    this.friends = res;
     console.log(this.user);
     console.log(this.friends);
    });
  }

  onOpenAddMenu() {
    this.showAddMenu = true;
  }

  onMenuBack() {
    this.showAddMenu = false;
  }

}
