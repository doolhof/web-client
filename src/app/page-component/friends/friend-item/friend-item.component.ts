import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../../model/user/user';

@Component({
  selector: 'app-friend-item',
  templateUrl: './friend-item.component.html',
  styleUrls: ['./friend-item.component.scss']
})
export class FriendItemComponent implements OnInit {
  @Input() friend: User;
  @Input() selected: boolean;

  constructor() { }

  ngOnInit() {
  }

}
