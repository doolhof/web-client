import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../../../model/user/user';

@Component({
  selector: 'app-match-item',
  templateUrl: './match-item.component.html',
  styleUrls: ['./match-item.component.scss']
})
export class MatchItemComponent implements OnInit {
  @Input() friend: User;
  @Input() selected: boolean;

  constructor() { }

  ngOnInit() {
  }

}
