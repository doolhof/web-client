import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFriendMenuComponent } from './add-friend-menu.component';

describe('AddFriendMenuComponent', () => {
  let component: AddFriendMenuComponent;
  let fixture: ComponentFixture<AddFriendMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFriendMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFriendMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
