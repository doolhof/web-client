import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {User} from '../../../../model/user/user';
import {UserService} from '../../../services/user.service';
import {FriendService} from '../../../services/friend.service';
import {NotifierService} from 'angular-notifier';
import {errorHandler} from '@angular/platform-browser/src/browser';
import {catchError} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-add-friend-menu',
  templateUrl: './add-friend-menu.component.html',
  styleUrls: ['./add-friend-menu.component.scss']
})
export class AddFriendMenuComponent implements OnInit {
  @Input() user: User;
  @Output() back = new EventEmitter();
  @Output() updateUser = new EventEmitter();
  matchingUser: User;
  selectedUser: User;
  hasSearched: boolean;

  constructor(private userService: UserService, private friendService: FriendService, private notifierService: NotifierService) {
    this.userService.getUserByUserId(this.userService.getLoggedInUser().userId).subscribe(res => this.user = res);
  }

  ngOnInit() {
    this.hasSearched = false;
  }

  onSelectMatch(user: User) {
    this.selectedUser = user;
  }

  onUnselect() {
    this.selectedUser = null;
  }

  onBack() {
    this.back.emit();
  }

  onAddFriend(friend: User) {
    console.log(this.selectedUser);
    console.log(this.user);
    this.friendService.addFriend(this.user.userId, friend.userId).subscribe(res => {
      this.updateUser.emit();
      this.matchingUser = null;
      this.selectedUser = null;
      console.log(res);
    });
    this.notifierService.notify('default', friend.username + ' added to friends list.');
  }

  search(event): void {
    this.matchingUser = null;
    this.userService.getUserByUsername(event).subscribe(res => {
      if (!this.user.friends.find(f => f.username === res.username)) {
        this.matchingUser = res;
      }
    });
    this.userService.getUserByUserId(this.userService.getLoggedInUser().userId).subscribe(res => this.user = res);
    console.log(this.user);
    this.hasSearched = true;
  }
}
