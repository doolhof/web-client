import {Component, HostListener, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LobbyService} from '../../services/lobby.service';
import {Lobby, LobbySocketMessage, User} from '../../../model/user/user';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {Howl} from 'howler';
import {GameService} from '../../services/game.service';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit {
  private lobbyStompClient;
  lobby: Lobby;
  private user: User;
  private socketMessage: LobbySocketMessage;
  private hostUser: User;
  lobbyId: number;
  plsNoRemoveLobby: boolean;
  sound = new Howl({
    src: ['assets/sounds/Sea_Shanty_2.mp3'], html5: true,
    autoplay: true,
    loop: true,
    volume: 1
  });


  constructor(private  lobbyService: LobbyService,
              private router: Router,
              private route: ActivatedRoute,
              private gameService: GameService) {
    this.route.paramMap.subscribe(params => {
      this.lobbyId = +params.get('id');
      this.lobbyService.getLobbyById(this.lobbyId).subscribe(res => {
        this.lobby = res;
        this.hostUser = this.lobby.host;
        this.lobbyConnect();
      });
    });
  }


  ngOnInit() {
    this.plsNoRemoveLobby = false;
  }

  lobbyConnect() {
    const that = this;
    const socket = new SockJS('http://localhost:9090/game-party-socket');
    this.lobbyStompClient = Stomp.over(socket);
    this.lobbyStompClient.connect({}, function () {
      that.lobbyStompClient.subscribe('/lobby/party', (res) => {
        if (res.body.toString() === 'true') {
          that.lobbyStompClient.disconnect();
        } else if (res.body.toString().substring(0, 5) === 'start') {
          that.router.navigate(['/game/' + res.body.toString().substring(6, 7)]);
        } else {
          that.lobby = JSON.parse(res.body);
        }
      });
      that.joinParty();
    });
  }

  joinParty() {
    this.user = JSON.parse(window.localStorage.getItem('currentUser'));
    this.socketMessage = new LobbySocketMessage(this.user.userId.toString(), this.lobby.lobbyId.toString());
    this.lobby = JSON.parse(this.lobbyStompClient.send('/party/join', {}, JSON.stringify(this.socketMessage)));
  }

  @HostListener('window:beforeunload')
  canDeactivate() {
    if (!this.plsNoRemoveLobby) {
      if (this.user && this.lobby.host.userId === this.user.userId) {
        this.lobbyStompClient.send('/party/leaveHost', {}, JSON.stringify(this.socketMessage));
        this.lobbyStompClient.disconnect();
      } else {
        this.lobbyStompClient.send('/party/leave', {}, JSON.stringify(this.socketMessage));
        this.lobbyStompClient.disconnect();
      }
    } else {
      this.lobbyStompClient.disconnect();
    }
  }

  onStartGame() {
    this.plsNoRemoveLobby = true;
    if (this.lobby.game.status !== 'WAITING') {
      // this.router.navigate(['/game/' + this.lobby.game.gameId]);
      this.socketMessage = new LobbySocketMessage(this.user.userId.toString(), this.lobby.lobbyId.toString());
      this.lobbyStompClient.send('/party/startGame', {}, JSON.stringify(this.socketMessage));
    } else {
      this.gameService.getNewGame(this.lobby.lobbyId).subscribe(res => {
          this.lobby.game = res;
        this.socketMessage = new LobbySocketMessage(this.user.userId.toString(), this.lobby.lobbyId.toString());
        this.lobbyStompClient.send('/party/startGame', {}, JSON.stringify(this.socketMessage));
        }
      );
    }
  }

  onJoinGame() {
    this.router.navigate(['/game/' + this.lobby.game.gameId]);
  }

  onLeave() {
    this.router.navigate(['/']);
  }

}
