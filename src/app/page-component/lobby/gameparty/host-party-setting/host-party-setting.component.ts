import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-host-party-setting',
  templateUrl: './host-party-setting.component.html',
  styleUrls: ['./host-party-setting.component.scss']
})
export class HostPartySettingComponent implements OnInit {
  @Input() online: number;
  @Input() total: number;
  constructor() { }

  ngOnInit() {
  }

}
