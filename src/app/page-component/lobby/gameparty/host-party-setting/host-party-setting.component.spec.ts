import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HostPartySettingComponent } from './host-party-setting.component';

describe('HostPartySettingComponent', () => {
  let component: HostPartySettingComponent;
  let fixture: ComponentFixture<HostPartySettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HostPartySettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostPartySettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
