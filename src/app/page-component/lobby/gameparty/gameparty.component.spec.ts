import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamepartyComponent } from './gameparty.component';

describe('GamepartyComponent', () => {
  let component: GamepartyComponent;
  let fixture: ComponentFixture<GamepartyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamepartyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamepartyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
