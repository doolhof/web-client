import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {User} from '../../../../model/user/user';

@Component({
  selector: 'app-gameparty',
  templateUrl: './gameparty.component.html',
  styleUrls: ['./gameparty.component.scss']
})
export class GamepartyComponent implements OnInit, OnChanges  {

  @Input() users: User[];
  @Input() hostUser: User;
  otherUsers: User[];
  @Input() maxUsers: number;

  constructor() {

  }

  ngOnInit() {
    this.users.forEach(x => console.log(x.username));
    this.otherUsers = this.users.filter(user => user.userId !== this.hostUser.userId);
    console.log(this.otherUsers);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['users'] && changes['users'].currentValue !== changes['users'].previousValue) {
      this.otherUsers = this.users.filter(user => user.userId !== this.hostUser.userId);
    }
  }

}
