import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../../../model/user/user';

@Component({
  selector: 'app-host-profile',
  templateUrl: './host-profile.component.html',
  styleUrls: ['./host-profile.component.scss']
})
export class HostProfileComponent implements OnInit {

  @Input() hostUser: User;
  constructor() { }

  ngOnInit() {
  }

}
