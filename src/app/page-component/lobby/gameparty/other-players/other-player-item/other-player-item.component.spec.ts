import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherPlayerItemComponent } from './other-player-item.component';

describe('OtherPlayerItemComponent', () => {
  let component: OtherPlayerItemComponent;
  let fixture: ComponentFixture<OtherPlayerItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherPlayerItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherPlayerItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
