import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../../../../model/user/user';

@Component({
  selector: 'app-other-player-item',
  templateUrl: './other-player-item.component.html',
  styleUrls: ['./other-player-item.component.scss']
})
export class OtherPlayerItemComponent implements OnInit {
  @Input() user: User;

  constructor() {
  }

  ngOnInit() {

  }
}
