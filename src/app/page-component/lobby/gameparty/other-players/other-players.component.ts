import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../../../model/user/user';

@Component({
  selector: 'app-other-players',
  templateUrl: './other-players.component.html',
  styleUrls: ['./other-players.component.scss']
})
export class OtherPlayersComponent implements OnInit {

  @Input() otherUsers: User[];

  constructor() {
  }

  ngOnInit() {
    console.log(this.otherUsers);
  }

}
