import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {User} from '../../../../model/user/user';
import {LobbyService} from '../../../services/lobby.service';
import {SelectableListItemDomain} from './list-area/list/list-item/selectableListItemDomain';
import {FriendService} from '../../../services/friend.service';

@Component({
  selector: 'app-invites',
  templateUrl: './invites.component.html',
  styleUrls: ['./invites.component.scss']
})
export class InvitesComponent implements OnInit, OnChanges {

  @Input() lobbyId: number;
  community: User[];
  friends: User[];
  private user: User;
  private tabIndex: number;
  private maxListCounter = 2;
  private listSelectableItem: SelectableListItemDomain[];

  /* private showTabs: { showUsername: boolean, showSocial: false, showFriends: false,
     showRecent: false, showAll: false, showComputer: false };*/

  constructor(private lobbyService: LobbyService, private friendService: FriendService) {
    this.user = JSON.parse(window.localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.lobbyService.getCommunity(this.user.userId, 0, this.maxListCounter).subscribe(res => {
      this.community = res;
      this.friendService.getFriendsByUser(this.user.userId).subscribe(f => this.friends = f);
    });
    // this.showTabs[1] = true;
    this.tabIndex = 2;
  }

  setSelectableItemsList($event) {
    this.listSelectableItem = $event;
  }

  changeTab(tabNr) {
    this.tabIndex = tabNr;
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

}
