import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {User} from '../../../../../model/user/user';
import {SelectableListItemDomain} from './list/list-item/selectableListItemDomain';
import {LobbyService} from '../../../../services/lobby.service';
import {FriendService} from '../../../../services/friend.service';

@Component({
  selector: 'app-list-area',
  templateUrl: './list-area.component.html',
  styleUrls: ['./list-area.component.scss']
})
export class ListAreaComponent implements OnInit, OnChanges {
  @Input() lobbyId: number;
  @Input() community: User[];
  @Input() cachedListSelectableItem: SelectableListItemDomain[];
  @Input() tabIndex: number;
  @Output() selectableItemsEmitter = new EventEmitter();
  searchResults: string;
  listSelectableItem: SelectableListItemDomain[];
  searchList: SelectableListItemDomain[] = [];
  user: User;

  constructor(private lobbyService: LobbyService, private friendService: FriendService) {
  }

  ngOnInit() {
    this.listSelectableItem = this.community.map(user => new SelectableListItemDomain(user, false));
    this.user = JSON.parse(window.localStorage.getItem('currentUser'));
  }

  showSearchResults(event) {
    this.searchResults = event;
    this.initLists();
  }

  setSelectableItemsList($event) {
    this.selectableItemsEmitter.emit($event);
  }


  initLists() {
    this.searchList = [];
    if (this.searchResults.length >= 1) {
      this.lobbyService.searchUsersByUsername(this.searchResults, this.user.userId).subscribe(res => {
        this.listSelectableItem = [];
        res.forEach(user => {
          if (!this.listSelectableItem.map(item => item.user.userId).includes(user.userId)) {
            this.listSelectableItem.push(new SelectableListItemDomain(user, false));
          }
        });
      });
    } else if (!this.cachedListSelectableItem) {
      this.listSelectableItem = this.community.map(user => new SelectableListItemDomain(user, false));
    } else {
      this.listSelectableItem = this.cachedListSelectableItem;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
  }
}
