import {User} from '../../../../../../../model/user/user';

export class SelectableListItemDomain {

    constructor(public user: User, public isSelected: Boolean = false) {
    }
}
