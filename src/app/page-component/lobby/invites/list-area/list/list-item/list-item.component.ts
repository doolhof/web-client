import {Component, Input, OnInit} from '@angular/core';
import {SelectableListItemDomain} from './selectableListItemDomain';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {

  @Input() set isSelected(object: boolean) {
    this.selected = object;
    this.toggle();
  }

  @Input() selectableItem: SelectableListItemDomain;
  @Input() name: string;
  @Input() index: Number;
  @Input() selectFunction: (number: Number) => {};
  text: string;

  private selected: boolean;

  public lottieConfig: Object;
  private anim: any;

  constructor() {
    this.lottieConfig = {
      path: 'assets/anim/checked-loading3.json',
      autoplay: false,
      loop: false
    };
  }


  animInit(anim: any) {
    this.anim = anim;
    this.anim.setSpeed(1);
  }

  celClicked = () => {
    this.selectFunction(this.index);
    console.log(this.index);
    this.toggle();
  }

  toggle() {
    if (this.anim) {
      this.selected ? this.anim.play() : this.anim.stop();
    }
  }

  ngOnInit(): void {
    if (this.selectableItem.isSelected) {
      this.anim.play();
      console.log('play');
    }
  }


}
