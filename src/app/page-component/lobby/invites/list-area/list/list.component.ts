import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {SelectableListItemDomain} from './list-item/selectableListItemDomain';
import {User} from '../../../../../../model/user/user';
import {LobbyService} from '../../../../../services/lobby.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnChanges {
  @Input() searchResults: string;
  @Input() lobbyId: number;
  @Input() selectableItems: SelectableListItemDomain[];
  @Input() isRadioButtonList: boolean;
  @Input() tabIndex: number;
  @Output() cellClick = new EventEmitter();
  @Output() selectableItemsEmitter = new EventEmitter();
  private longerPage: SelectableListItemDomain[];
  private maxListCounter = 1;
  private user: User;

  constructor(private  lobbyService: LobbyService) {
    this.user = JSON.parse(window.localStorage.getItem('currentUser'));
  }

  ngOnInit() {
  }

  selectableItemClick = (index: number) => {
    if (this.isRadioButtonList) {
      this.clearSelected();
      return this.selectableItems[index].isSelected = true;
    }
    this.selectableItems[index].isSelected = !this.selectableItems[index].isSelected;
  }

  private clearSelected = () => {
    for (let i = 0; i < this.selectableItems.length; i++) {
      this.selectableItems[i].isSelected = false;
    }
  }

  sendInvitations() {
    this.lobbyService.sendInvitation(this.selectableItems.filter(item => item.isSelected)
      .map(item => item.user.userId.toString()), this.lobbyId).subscribe(res => {
    });
  }


  loadMoreUsers() {
    this.maxListCounter += 3;
    this.lobbyService.getCommunity(this.user.userId, 0, this.maxListCounter).subscribe(res => {
      console.log(res);
      if (res.length >= 1) {
        res.forEach(user => {
          console.log(user);
          if (!this.selectableItems.map(item => item.user.userId).includes(user.userId)) {
            this.selectableItems.push(new SelectableListItemDomain(user, false));
          }
        });
       // this.selectableItems = this.selectableItems.filter(item => res.includes(item.user));
        //    this.selectableItems = this.selectableItems.concat(res.map(user => new SelectableListItemDomain(user, false)));
        this.selectableItemsEmitter.emit(this.selectableItems);
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

}
