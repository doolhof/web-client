import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-button-invite',
  templateUrl: './button-invite.component.html',
  styleUrls: ['./button-invite.component.scss']
})
export class ButtonInviteComponent implements OnInit {

  @Input() label: string;

  constructor() {
  }

  ngOnInit() {
  }

}
