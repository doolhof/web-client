import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonInviteComponent } from './button-invite.component';

describe('ButtonInviteComponent', () => {
  let component: ButtonInviteComponent;
  let fixture: ComponentFixture<ButtonInviteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonInviteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonInviteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
