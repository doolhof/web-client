import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {User} from '../../../model/user/user';
import {Router} from '@angular/router';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: User = JSON.parse(window.localStorage.getItem('currentUser'));
  incomingMessage = this.user.userSetting.incomingMessage;
  myTurn = this.user.userSetting.myTurn;
  turnExpiring = this.user.userSetting.turnExpiring;
  receiveInvite = this.user.userSetting.receiveInvite;
  turnPlayed = this.user.userSetting.turnPlayed;
  showEditPassword: boolean;
  showDeleteAccount: boolean;
  constructor(private  userService: UserService, private router: Router) {
    this.updateUser();
    this.user = JSON.parse(window.localStorage.getItem('currentUser'));
  }

  ngOnInit() {
    this.showEditPassword = false;
    this.showDeleteAccount = false;
    this.updateUser();
  }
  checkClickedMyTurn(val) {
    if (val) {
      this.myTurn = false;
      this.user.userSetting.myTurn = this.myTurn;
      this.userService.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    } else {
      this.myTurn = true;
      this.user.userSetting.myTurn = this.myTurn;
      this.userService.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    }
  }

  checkClickedIncomingMessage(val) {
    if (val) {
      this.incomingMessage = false;
      this.user.userSetting.incomingMessage = this.incomingMessage;
      this.userService.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    } else {
      this.incomingMessage = true;
      this.user.userSetting.incomingMessage = this.incomingMessage;
      this.userService.updateUser(this.user).subscribe(res => {
        this.user = res;
      });
    }
  }

  checkClickedTurnExpiring(val) {
    if (val) {
      this.turnExpiring = false;
      this.user.userSetting.turnExpiring = this.turnExpiring;
      this.userService.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    } else {
      this.turnExpiring = true;
      this.user.userSetting.turnExpiring = this.turnExpiring;
      this.userService.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    }
  }


  checkClickedReceiveInvite(val) {
    if (val) {
      this.receiveInvite = false;
      this.user.userSetting.receiveInvite = this.receiveInvite;
      this.userService.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    } else {
      this.receiveInvite = true;
      this.user.userSetting.receiveInvite = this.receiveInvite;
      this.updateUser();
    }
  }

  checkClickedTurnPlayed(val) {
    if (val) {
      this.turnPlayed = false;
      this.user.userSetting.turnPlayed = this.turnPlayed;
      this.userService.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    } else {
      this.turnPlayed = true;
      this.user.userSetting.turnPlayed = this.turnPlayed;
      this.userService.updateUser(this.user).subscribe(res => {
        this.user = res;
        this.updateUser();
      });
    }
  }
  onChangePassword() {
    this.showEditPassword = true;
  }
  onDeleteAccount() {
    this.showDeleteAccount = true;
  }

  onBack() {
    this.showEditPassword = false;
    this.showDeleteAccount = false;
  }
  onBackMain() {
    this.router.navigateByUrl('/mainmenu');
  }
  updateUser() {
    this.userService.getUserByUserId(this.user.userId).subscribe(res => {
      window.localStorage.setItem('currentUser', JSON.stringify(res));
      this.user = JSON.parse(window.localStorage.getItem('currentUser'));
    });
  }
}

