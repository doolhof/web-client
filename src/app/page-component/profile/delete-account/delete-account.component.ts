import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PasswordDto, User} from '../../../../model/user/user';
import {UserService} from '../../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-delete-account',
  templateUrl: './delete-account.component.html',
  styleUrls: ['./delete-account.component.scss']
})
export class DeleteAccountComponent implements OnInit {
  @Output() back = new EventEmitter();
  user: User = JSON.parse(window.localStorage.getItem('currentUser'));

  constructor(private  userService: UserService, private router: Router) { }

  ngOnInit() {
  }
  onBack() {
    this.back.emit();
  }
  onYes() {
    this.userService.deleteAccount(this.user.userId).subscribe(res => {
      console.log((res.valueOf()));
    });
    this.userService.logout();
    this.router.navigateByUrl('/login');
    this.back.emit();
  }
}
