import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {PasswordDto, User} from '../../../../model/user/user';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-edit-password',
  templateUrl: './edit-password.component.html',
  styleUrls: ['./edit-password.component.scss']
})
export class EditPasswordComponent implements OnInit {
  @Output() back = new EventEmitter();
  user: User = JSON.parse(window.localStorage.getItem('currentUser'));
  oldPassword: string;
  newPassword: string;
  passwordDto: PasswordDto;
  constructor(private  userService: UserService) {}

  ngOnInit() {
  }
  onBack() {
    this.back.emit();
  }
  readOldPassword(password): void {
    console.log(password);
    this.oldPassword = password;
  }
  readNewPassword(password): void {
    console.log(password);
    this.newPassword = password;
  }
  onConfirm() {
    this.passwordDto = new PasswordDto(this.user.userId, this.oldPassword, this.newPassword);
    this.userService.updatePassword(new PasswordDto(this.user.userId, this.oldPassword, this.newPassword))
      .subscribe(res => {
      console.log((res.valueOf()));
    });
    this.back.emit();
  }
}
