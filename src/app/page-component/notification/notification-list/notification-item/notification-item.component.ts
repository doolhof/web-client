import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from '../../../../services/notification.service';

@Component({
  selector: 'app-notification-item',
  templateUrl: './notification-item.component.html',
  styleUrls: ['./notification-item.component.scss']
})
export class NotificationItemComponent implements OnInit {
  @Input() notification;
  @Output() closeNotif = new EventEmitter();

  constructor(private router: Router,
              private route: ActivatedRoute,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
  }

  goToLobby() {
    console.log('in goToLobby');
    this.closeNotif.emit();
    console.log(this.notification.webUrl);
    this.router.navigate(['/lobby', this.notification.webUrl]/*, {relativeTo: this.route}*/);
  }

  closeNotification() {
    console.log('in closeNotIf');
    this.closeNotif.emit();
    this.notificationService.putNotificationToClose(this.notification.notificationId).subscribe(e => console.log(e));
  }
}
