import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Notificat} from '../../../../model/user/user';

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.scss']
})
export class NotificationListComponent implements OnInit {
  @Output() close = new EventEmitter();
  @Input() notifications;
  constructor() {
  }

  ngOnInit() {
  }

  onClose() {
    this.close.emit();
  }

  onCloseIt($event) {
    this.close.emit();
  }
}
