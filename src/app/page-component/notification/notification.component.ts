import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../../services/notification.service';
import {Notificat, User} from '../../../model/user/user';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFireMessaging} from '@angular/fire/messaging';
import {BehaviorSubject} from 'rxjs';
import {take} from 'rxjs/operators';
import {UserService} from '../../services/user.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  message;
  inlogUser: User = JSON.parse(window.localStorage.getItem('currentUser'));
  currentMessage = new BehaviorSubject(null);
  notificationList: Notificat[];
  showNotifications: boolean;

  /*   NOG TE VERWIJDEREN */
  constructor(
    private angularFireDB: AngularFireDatabase,
    private angularFireAuth: AngularFireAuth,
    private angularFireMessaging: AngularFireMessaging,
    private userService: UserService, private notifierService: NotifierService,
    private notificationService: NotificationService) {
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
        console.log(_messaging);
      }
    );
  }

  ngOnInit() {
    this.showNotifications = false;
    this.getNotifications();
    const userId = this.inlogUser.userId;
    this.requestPermission(userId);
    this.receiveMessage();
    this.message = this.currentMessage;
  }

  /**
   * update token in firebase database
   *
   * @param userId userId as a key
   * @param token token as a value
   */
  updateToken(userId, token) {
    this.angularFireAuth.authState.pipe(take(1)).subscribe(
      () => {
        const data = {};
        data[userId] = token;
        this.angularFireDB.object('fcmTokens/').update(data);
        this.inlogUser.fcmToken = token;
        this.userService.updateFcmToken(this.inlogUser).subscribe(res => {
        });
      });
  }

  /**
   * request permission for notification from firebase cloud messaging
   *
   * @param userId userId
   */
  requestPermission(userId) {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log(token);
        this.updateToken(userId, token);
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }

  /**
   * hook method when new notification received in foreground
   */
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        if (this.inlogUser.userSetting.receiveInvite) {
          const jsonMsg = JSON.stringify(payload).split('"');
          for (let i = 0; i < jsonMsg.length; i++) {
            if (jsonMsg[i] === 'body') {
              console.log(jsonMsg[i + 2]);
              this.notifierService.notify('default', jsonMsg[i + 2]);
            }
          }
          console.log('new message received. ', payload);
          this.getNotifications();
        }
      });
  }

  getNotifications() {
    this.notificationService.getAllNotifications(this.inlogUser.userId).subscribe(res => {
      this.notificationList = res;
      console.log(res);
      console.log(this.inlogUser);
    });
  }

  onShowNotifications() {
    console.log('in onShowNotifications');
    this.showNotifications = true;
  }

  onClose() {
    this.getNotifications();
    this.showNotifications = false;
  }
}
