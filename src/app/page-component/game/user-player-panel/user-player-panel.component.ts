import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../../../model/game/game';

@Component({
  selector: 'app-user-player-panel',
  templateUrl: './user-player-panel.component.html',
  styleUrls: ['./user-player-panel.component.scss']
})

export class UserPlayerPanelComponent implements OnInit {
  @Input() player: Player;

  constructor() {
  }

  ngOnInit() {
  }
}
