import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPlayerPanelComponent } from './user-player-panel.component';

describe('UserPlayerPanelComponent', () => {
  let component: UserPlayerPanelComponent;
  let fixture: ComponentFixture<UserPlayerPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPlayerPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPlayerPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
