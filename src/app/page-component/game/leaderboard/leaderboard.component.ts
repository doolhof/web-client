import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss']
})
export class LeaderboardComponent implements OnInit {
  @Input() victorName;

  constructor(private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
  }

  onMainMenu() {
    this.router.navigateByUrl('/');
  }
}
