import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {Turn} from '../../../../model/game/game';

@Component({
  selector: 'app-turn-status',
  templateUrl: './turn-status.component.html',
  styleUrls: ['./turn-status.component.scss']
})
export class TurnStatusComponent implements OnChanges {
  timeLeft;
  interval;
  timeIsUp: boolean;
  @Output() stopTurn = new EventEmitter();
  @Input() btnShow;
  @Input() turn: Turn;
  @Input() replayMode: false;


  ngOnChanges(changes: SimpleChanges): void {
    clearInterval(this.interval);
    this.startTimer();
  }

  startTimer() {
    if (!this.replayMode) {
      this.timeIsUp = false;
      this.timeLeft = this.turn.turnDuration;
      this.interval = setInterval(() => {
        if (this.timeLeft > 0) {
          this.timeLeft--;
        } else {
          console.log('time is up');
          this.timeIsUp = true;
          this.onTurnDone();
        }
      }, 1000);
    }

  }

  stopTimer() {
    clearInterval(this.interval);
    console.log('time left: ' + this.timeLeft);
  }

  onTurnDone() {
    this.stopTimer();
    this.stopTurn.emit(this.timeLeft);
  }
}
