import {Component, OnInit, ViewChild} from '@angular/core';
import {Game, MazeCard, Player, PlayerSocketMessage, Tile, TileSocketMessage, Treasure, Turn} from '../../../model/game/game';
import {Howl} from 'howler';
import {GameboardComponent} from './gameboard/gameboard.component';
import {TurnStatusComponent} from './turn-status/turn-status.component';
import {User} from '../../../model/user/user';
import {ActivatedRoute, Router} from '@angular/router';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import * as GameUtil from '../../../util/gameUtil';
import {GameService} from '../../services/game.service';
import {TurnService} from '../../services/turn.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  user: User = JSON.parse(window.localStorage.getItem('currentUser'));
  game: Game;
  remainingMazeCard: MazeCard;
  myPlayer: Player;
  otherPlayers: Player[];
  playerTurn: Player;
  turn = new Turn(10, 10, 'CARDMOVE', new Date(), [], 0, 0, -90, 'player');
  turns: Turn[];
  showMenu: boolean;
  replayCounter = 0;
  @ViewChild(GameboardComponent) gameboardComponent: GameboardComponent;
  @ViewChild(TurnStatusComponent) turnStatusComponent: TurnStatusComponent;
  sound = new Howl({src: ['assets/sounds/fightmusic.mp3'], html5: true, autoplay: true, loop: true, volume: 0.1});
  gameStompClient;
  victorName = '';
  showVictory: Boolean;
  replayMode = false;

  constructor(private router: Router, private route: ActivatedRoute, private _gameService: GameService, private turnService: TurnService) {
    if (this.router.url.includes('/replay/')) {
      this.replayMode = true;
    }
    this.gameConnect();
  }

  ngOnInit() {
    this.sound.play();
    this.showMenu = false;
    this.otherPlayers = null;
    this.myPlayer = null;
    this.playerTurn = null;
  }

  gameConnect() {
    const that = this;
    const socket = new SockJS('http://localhost:9090/game-socket'); // Socket in config van back-end
    this.gameStompClient = Stomp.over(socket);
    this.gameStompClient.connect({}, function () {
      that.getGameSubscription();
      that.getPlayerSubscription();
      that.movePlayerSubscription();
      that.moveMazeCardSubscription();
      that.endTurnSubscription();
      that.getWinGameSubscription();
      GameUtil.getGame(that.route.snapshot.paramMap.get('id'), that.user.userId, that.gameStompClient);
    });
  }

  getWinGameSubscription() {
    this.gameStompClient.subscribe('/game/checkWinGame', res => {
      this.victorName = res.body;
      this.showVictory = true;
    });
  }

  getGameSubscription() { // Method for getGame websocket
    this.gameStompClient.subscribe('/game/gameParty', res => {
      this.game = JSON.parse(res.body);
      this.game.playerList = this.game.playerList.map(player => new Player(player.playerId, player.color, player.name, player.toFind,
        player.found, player.turns, player.currentPosition, player.startPosition, player.turn));
      this.game.gameBoard.tiles = this.game.gameBoard.tiles.map(
        tile => new Tile(tile.tileId, tile.position, tile.mazeCard, tile.moveable));
      console.log(this.game);
      this.remainingMazeCard = this.game.gameBoard.tiles[49].mazeCard;
      this.myPlayer = this.game.playerList.filter(player => player.name === this.user.username)[0];
      this.otherPlayers = this.game.playerList.filter(p => this.myPlayer.playerId !== p.playerId);
      this.game.gameBoard.tiles = this.game.gameBoard.tiles.slice(0, -1);
      this.playerTurn = this.game.playerList[0];   // Players who's turn it is
      this.sortTreasures();
      this.turn.turnDuration = this.game.turnTime;
    });
  }

  getPlayerSubscription() {
    this.gameStompClient.subscribe('/game/getPlayer', res => {
      const tempPlayer = JSON.parse(res.body);
      if (tempPlayer.name === this.myPlayer.name) {
        this.myPlayer = tempPlayer;
      } else {
        for (let _i = 0; _i < this.otherPlayers.length; _i++) {
          if (this.otherPlayers[_i].name === tempPlayer.name) {
            this.otherPlayers[_i] = tempPlayer;
          }
        }
      }
    });
  }

  movePlayerSubscription() {
    this.gameStompClient.subscribe('/game/movePlayerTile', res => {
      console.log(JSON.parse(res.body));
      this.game.playerList = JSON.parse(res.body);
      this.myPlayer = this.game.playerList.filter(player => player.name === this.user.username)[0];
      this.otherPlayers = this.game.playerList.filter(p => this.myPlayer.playerId !== p.playerId);
    });
  }

  moveMazeCardSubscription() {
    this.gameStompClient.subscribe('/game/moveMazeCard', res => {
      this.game.gameBoard = JSON.parse(res.body);
      this.remainingMazeCard = this.game.gameBoard.tiles[49].mazeCard;
      this.game.gameBoard.tiles = this.game.gameBoard.tiles.slice(0, -1);
    });
  }

  endTurnSubscription() { // Method for endturn websocket
    this.gameStompClient.subscribe('/game/endTurn', res => {
      this.setNextPlayerTurn();
      this.turn.playerMoves = [];
    });
  }

  sortTreasures() {
    this.myPlayer.toFind = this.myPlayer.toFind.sort((a, b) => b.treasureId - a.treasureId).reverse();
  }

  onEndTurn(timeLeft: number): void {
    this.turn.turnDuration = timeLeft;
    this.turn.player = this.playerTurn.playerId;
    this.gameStompClient.send('/gameParty/endTurn', {}, JSON.stringify(this.turn));
  }

  updateTurnOrientation(orientation: number) {
    this.turn.orientation = orientation;
  }

  /**Change name*/
  onUpdateTurn(turn: Turn) {
    this.turn.turnPhase = turn.turnPhase;
    this.turn.posMazeCardMove = turn.posMazeCardMove;
    this.turn.playerMoves = turn.playerMoves;
  }

  setNextPlayerTurn() {
    console.log(this.game.playerList);
    const x = GameUtil.getNextPlayer(this.game.playerList, this.playerTurn);
    this.game.playerList[x].turn = true;
    this.playerTurn = this.game.playerList[x];
    this.turn.turnDuration = this.game.turnTime;
    this.gameboardComponent.updateTurn();
  }

  onMenu() {
    this.showMenu = true;
  }

  onBack() {
    this.showMenu = false;
  }

  private async replayGame(event) {
    if (event === true && this.replayMode === true) {
      this.turnService.getTurns(this.game.originalGame).subscribe(turns => {
        this.turns = turns;
        this.loopTurns();
      });
    }

  }

  private async loopTurns() {
    let playerId = 0;
    const that = this;
    for (let i = 0; i < this.turns.length; i++) {
      playerId = this.game.playerList.filter(res => res.name === that.turns[i].playerName).map(player => player.playerId)[0];
      await this.delay(1000);
      this.remainingMazeCard.orientation = that.turns[i].orientation;
      await this.delay(500);
      this.gameStompClient.send('/gameParty/updateRemaining', {}, JSON.stringify(this.remainingMazeCard));
      const tileSocketMessage = new TileSocketMessage(that.turns[i].posMazeCardMove.toString(),
        that.game.gameBoard.gameBoardId.toString());
      that.gameStompClient.send('/gameParty/moveMazeCard', {}, JSON.stringify(tileSocketMessage));
      const playerSocketMessage = new PlayerSocketMessage(playerId.toString(), that.turns[i].posMazeCardMove.toString());
      that.gameStompClient.send('/gameParty/movePlayerTile', {}, JSON.stringify(playerSocketMessage));
      await this.delay(1000);
      const playerSocketMessage2 = new PlayerSocketMessage(playerId.toString(),
        that.turns[i].playerMoves[that.turns[i].playerMoves.length - 1].toString());
      that.gameStompClient.send('/gameParty/move', {}, JSON.stringify(playerSocketMessage2));
      that.gameStompClient.send('/gameParty/treasureSearch', {}, JSON.stringify(playerSocketMessage2));
    }
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
