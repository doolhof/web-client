import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

export let x = 0;
export let y = 0;

@Component({
  selector: 'app-pawn',
  templateUrl: './pawn.component.html',
  styleUrls: ['./pawn.component.scss'],
  /*animations: [
    trigger('movePawn', [
      transition('none=>left, *=>left,  up=>left, down=>left, right=>left',
        animate('1s ease-in-out', style({transform: 'translate(' + x + 'px,' + y + 'px)'}))),
      transition('none=>right, *=>right,  up=>right, left=>right, down=>right',
        animate('1s ease-in-out', style({transform: 'translate(' + x + 'px,' + y + 'px)'}))),
      transition('none=>up,  *=>up,  down=>up, left=>up, right=>up',
        animate('1s ease-in-out', style({transform: 'translate(' + x + 'px,' + y + 'px)'}))),
      transition('none=>down, *=>down, up=>down, left=>down, right=>down',
        animate('1s ease-in-out', style({transform: 'translate(' + x + 'px,' + y + 'px)'})))
    ])]
*/
})

export class PawnComponent implements OnInit, OnChanges {
  @Input() color: string;
  @Input() steps: string[];
  state = 'none';
  counter = 0;
  imageSource: string;

  constructor() {
  }

  ngOnInit() {
    this.imageSource = '../../../../../assets/img/players/idle_' + this.color + '.gif';
  }

  animationStart(event) {
    if (this.state === 'left') {
      x = x - 40;
    } else if (this.state === 'right') {
      x = x + 40;
    } else if (this.state === 'up') {
      y = y - 40;
    } else if (this.state === 'down') {
      y = y + 40;
    }
    console.log('started');
    console.log(x + '//' + y);
  }

  animationEnd(event) {
    console.log(event);
    this.state = this.steps[this.counter];
    this.counter++;
  }

  ngOnChanges(changes: SimpleChanges): void {
    // if (this.steps.length !== 0) {
    //   this.state = this.steps[0];
    // }
  }
}
