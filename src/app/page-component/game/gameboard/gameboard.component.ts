import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Game, Player, PlayerSocketMessage, Tile, TileSocketMessage, Turn} from '../../../../model/game/game';
import {GameService} from '../../../services/game.service';
import {TokenStorage} from '../../../core/token.storage';
import {ActivatedRoute, Router} from '@angular/router';
import {PlayerService} from '../../../services/player.service';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material';

@Component({
  selector: 'app-gameboard',
  templateUrl: './gameboard.component.html',
  styleUrls: ['./gameboard.component.scss']
})
export class GameboardComponent implements OnInit, OnChanges {
  @Input() game: Game;
  @Input() myPlayer: Player;
  @Input() otherPlayers: Player[];
  @Input() gameStompClient;
  @Input() replayMode: boolean;
  pathTiles: Tile[];
  lockedArrow: number;
  steps: string[];
  @Input() playerTurn: Player;
  @Output() updatePlayerTurn = new EventEmitter();
  @Output() replayReady = new EventEmitter();
  turn = new Turn(10, 10, 'CARDMOVE', new Date(), [], 0, 0, 0, 'player');
  infoMsg: string;


  constructor(private _gameservice: GameService,
              private tokenstorage: TokenStorage,
              private router: Router,
              private route: ActivatedRoute,
              private playerService: PlayerService,
              private iconRegistry: MatIconRegistry,
              private sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon('arrowDown', sanitizer.bypassSecurityTrustResourceUrl('../../../../assets/img/arrows/arrowDown.svg'));
    iconRegistry.addSvgIcon('arrowUp', sanitizer.bypassSecurityTrustResourceUrl('../../../../assets/img/arrows/arrowUp.svg'));
    iconRegistry.addSvgIcon('arrowLeft', sanitizer.bypassSecurityTrustResourceUrl('../../../../assets/img/arrows/arrowLeft.svg'));
    iconRegistry.addSvgIcon('arrowRight', sanitizer.bypassSecurityTrustResourceUrl('../../../../assets/img/arrows/arrowRight.svg'));
  }

  public sortByPosition() {
    this.game.gameBoard.tiles.sort((a, b) => b.position - a.position).reverse();
  }

  ngOnInit() {
    this.sortByPosition();
    this.gameStompClient.subscribe('/game/disabledArrow', res => {
      this.lockedArrow = JSON.parse(res.body);
    });
    this.gameStompClient.subscribe('/game/movePawn', res => {
      this.pathTiles = JSON.parse(res.body);
      this.pathTiles.forEach(obj => {
        this.turn.playerMoves.push(obj.position);
      });
      this.gameStompClient.send('/gameParty/getPlayer', {}, JSON.stringify(this.myPlayer.playerId));
    });
    this.replayReady.emit(true);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.turn.turnPhase === 'CARDMOVE') {
      this.infoMsg = 'place the mazecard.';
    } else {
      this.infoMsg = 'move your player.';
    }
  }


  /**This method searches for the arrow button that must be locked (opposite side)*/
  lockArrow(number: any) {
    this.gameStompClient.send('/gameParty/disabledArrow', {}, JSON.stringify(number));
  }


  /**Method for moving a tile on the gameBoard*/
  moveTile(number: number) {
     if (this.turn.turnPhase !== 'PLAYERMOVE') {
    console.log('move tile');
    this.turn.posMazeCardMove = number;
    this.turn.turnPhase = 'PLAYERMOVE';
    this.lockArrow(number);
    const tileSocketMessage = new TileSocketMessage(number.toString(), this.game.gameBoard.gameBoardId.toString());
    this.gameStompClient.send('/gameParty/moveMazeCard', {}, JSON.stringify(tileSocketMessage));
    const playerSocketMessage = new PlayerSocketMessage(this.myPlayer.playerId.toString(), number.toString());
    this.gameStompClient.send('/gameParty/movePlayerTile', {}, JSON.stringify(playerSocketMessage));
      }
  }

  movePawn(destination: number) {
    if (this.turn.turnPhase !== 'CARDMOVE') {
      const playerSocketMessage = new PlayerSocketMessage(this.myPlayer.playerId.toString(), destination.toString());
      this.gameStompClient.send('/gameParty/move', {}, JSON.stringify(playerSocketMessage));
      this.gameStompClient.send('/gameParty/treasureSearch', {}, JSON.stringify(playerSocketMessage));
      this.gameStompClient.send('/gameParty/checkWinGame', {}, JSON.stringify(this.myPlayer.playerId));
      this.updatePlayerTurn.emit(this.turn);
    }
  }

  updateTurn() {
    this.turn = new Turn(10, 10, 'CARDMOVE', new Date(), [], 0, 0, 0, 'player');
  }
}

