import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GameboardComponent} from './gameboard.component';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {TokenStorage} from '../../../core/token.storage';
import {GameService} from '../../../services/game.service';
import {PlayerService} from '../../../services/player.service';
import {MatIconModule, MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {TileComponent} from './tile/tile.component';
import {PawnComponent} from './pawn/pawn.component';
import {TreasureComponent} from './treasure/treasure.component';
import {Game} from '../../../../model/game/game';
import {MockComponent} from 'ng-mocks';

describe('GameboardComponent', () => {
  let gameService;
  let playerService;
  let component;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MockComponent(GameboardComponent)],
      imports: [RouterTestingModule, HttpClientModule, MatIconModule],
      providers: [TokenStorage]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    gameService = TestBed.get(GameService);
    playerService = TestBed.get(PlayerService);
    component = new GameboardComponent(gameService, TestBed.get(TokenStorage),
      TestBed.get(Router), TestBed.get(ActivatedRoute),
      playerService, TestBed.get(MatIconRegistry), TestBed.get(DomSanitizer));
  });

  it('turnPhase is CARDMOVE', () => {
    expect(component.turn.turnPhase).toBe('CARDMOVE');
  });
  it('LockArrow of 4 should be 46', () => {
    component.lockArrow(4);
    expect(component.lockedArrow).toBe(46);
  });
});
