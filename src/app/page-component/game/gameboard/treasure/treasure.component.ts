import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-treasure',
  templateUrl: './treasure.component.html',
  styleUrls: ['./treasure.component.scss']
})
export class TreasureComponent implements OnInit {
  @Input() treasure: string;
  imageSource: string;

  constructor() {}

  ngOnInit() {
    this.imageSource = '../../../../../assets/img/treasures/' + this.treasure + '.svg';
  }

}
