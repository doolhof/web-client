import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {MazeCard, Player, Tile, Treasure} from '../../../../../model/game/game';
import {state, style, trigger} from '@angular/animations';
import {PlayerService} from '../../../../services/player.service';

/**Animation trigger for rotation, this trigger is placed on the tile image, during the initialisation we will check on the state
 * of the tile*/
@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss'],
  animations: [
    trigger('rotatedState', [
      state('0', style({transform: 'rotate(0)'})),
      state('90', style({transform: 'rotate(90deg)'})),
      state('180', style({transform: 'rotate(180deg)'})),
      state('270', style({transform: 'rotate(270deg)'})),
    ])
  ]
})
export class TileComponent implements OnInit, OnChanges {
  @Input() tile: Tile;
  @Input() players: Player[];
  @Input() myPlayer: Player;
  @Input() otherPlayers: Player[];
  @Input() steps: String[];
  @Input() gameId: number;
  rotationState: string;
  mazeCardSource: string;
  @Input() mazeCard: MazeCard = {
    mazeCardId: 2,
    orientation: 90,
    northWall: false,
    eastWall: false,
    southWall: false,
    westWall: true,
    imageType: 2,
    treasure: new Treasure(1, 'earth')
  };


  constructor(private  playerService: PlayerService) {
  }

  ngOnInit() {
    this.rotationState = this.mazeCard.orientation.toString();
    this.mazeCardSource = '../../../../../assets/img/mazecards/' + this.mazeCard.imageType + '_alt2.png';
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.playerService.getAllPlayers(this.gameId).subscribe(res => {
      this.players = res;
    });
  }
}
