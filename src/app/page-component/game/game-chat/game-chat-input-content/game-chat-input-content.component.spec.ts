import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameChatInputContentComponent } from './game-chat-input-content.component';

describe('GameChatInputContentComponent', () => {
  let component: GameChatInputContentComponent;
  let fixture: ComponentFixture<GameChatInputContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameChatInputContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameChatInputContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
