import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameChatInputboxComponent } from './game-chat-inputbox.component';

describe('GameChatInputboxComponent', () => {
  let component: GameChatInputboxComponent;
  let fixture: ComponentFixture<GameChatInputboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameChatInputboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameChatInputboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
