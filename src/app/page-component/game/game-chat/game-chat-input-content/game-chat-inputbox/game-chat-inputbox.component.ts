import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-game-chat-inputbox',
  templateUrl: './game-chat-inputbox.component.html',
  styleUrls: ['./game-chat-inputbox.component.scss']
})
export class GameChatInputboxComponent implements OnInit {

  @ViewChild('textArea') textInput: ElementRef;
  @Output() messageEmitter: EventEmitter<string> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  sendMessage($event) {
    this.messageEmitter.emit($event);
    this.textInput.nativeElement.value = '';
  }

}
