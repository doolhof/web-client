import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-game-chat-input-content',
  templateUrl: './game-chat-input-content.component.html',
  styleUrls: ['./game-chat-input-content.component.scss']
})
export class GameChatInputContentComponent implements OnInit {

  @Output() messageEmitter: EventEmitter<string> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  sendMessage($event) {
    this.messageEmitter.emit($event);
  }

}
