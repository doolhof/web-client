import {Component, Input, OnInit} from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {ChatSocketMessage, User} from '../../../../model/user/user';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-game-chat',
  templateUrl: './game-chat.component.html',
  styleUrls: ['./game-chat.component.scss']
})
export class GameChatComponent implements OnInit {

  @Input() gameId: number;
  private chatStompClient;
  messageText = '';
  private user: User;
  private chatSocketMessage: ChatSocketMessage;
  private responseMessage: any;
  private lobbyId: number;

  constructor(private router: Router,
              private route: ActivatedRoute) {
    this.route.paramMap.subscribe(params => {
      this.lobbyId = +params.get('id');
    });
  }

  ngOnInit() {
    this.user = JSON.parse(window.localStorage.getItem('currentUser'));
    this.chatConnect();
  }

  chatConnect() {
    const that = this;
    const socket = new SockJS('http://localhost:9090/chat-socket');
    this.chatStompClient = Stomp.over(socket);
    this.chatStompClient.connect({}, function () {
      that.chatStompClient.send('/app/chat.addUser',
        {},
        JSON.stringify({sender: that.user.username, type: 'JOIN', lobbyId: that.lobbyId}));
      that.chatStompClient.subscribe('/topic/public', (response) => {
        that.onMessageReceived(response);
      });
    });
  }

  onMessageReceived(payload) {
    this.responseMessage = JSON.parse(payload.body);
    console.log(this.lobbyId);
    console.log(this.responseMessage.lobbyId);
    if (this.responseMessage.lobbyId.toString() === this.lobbyId.toString()) {
      if (this.responseMessage.type.toString() === 'JOIN') {
        if (this.responseMessage.content !== undefined) {
          this.responseMessage.content = this.responseMessage.sender + ' joined!';
          this.messageText = this.responseMessage.content;
        }
      } else if (this.responseMessage.type.toString() === 'LEAVE') {
        console.log('LEAVE');
        this.responseMessage.content = this.responseMessage.sender + ' left!';
        this.messageText = this.responseMessage.content;
      } else {
        this.messageText = this.responseMessage.sender + ': ' + this.responseMessage.content;
      }
    }
  }

  sendMessage($event) {
    const chatMessage = {
      sender: this.user.username,
      content: $event.toString(),
      type: 'CHAT',
      lobbyId: this.lobbyId.toString(),
      gameId: '0'
    };
    console.log(JSON.stringify(chatMessage));
    this.chatStompClient.send('/app/chat.sendMessage', {}, JSON.stringify(chatMessage));
  }

}
