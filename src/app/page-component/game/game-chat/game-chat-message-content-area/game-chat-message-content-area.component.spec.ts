import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameChatMessageContentAreaComponent } from './game-chat-message-content-area.component';

describe('GameChatMessageContentAreaComponent', () => {
  let component: GameChatMessageContentAreaComponent;
  let fixture: ComponentFixture<GameChatMessageContentAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameChatMessageContentAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameChatMessageContentAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
