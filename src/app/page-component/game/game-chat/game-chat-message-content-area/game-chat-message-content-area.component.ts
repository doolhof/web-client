import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-game-chat-message-content-area',
  templateUrl: './game-chat-message-content-area.component.html',
  styleUrls: ['./game-chat-message-content-area.component.scss']
})
export class GameChatMessageContentAreaComponent implements OnInit, OnChanges {

  @Input() newMessage = '';
  oldText = '';

  constructor() {
  }

  ngOnInit() {
    if (this.newMessage.length > 1) {
      this.oldText += `<br></br>${this.newMessage}`;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.newMessage.length > 1) {
      this.oldText += `<br></br>${this.newMessage}`;
    }
  }

}
