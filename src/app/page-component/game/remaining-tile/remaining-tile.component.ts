import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Game, MazeCard} from '../../../../model/game/game';
import {animate, state, style, transition, trigger} from '@angular/animations';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';

@Component({
  selector: 'app-remaining-tile',
  templateUrl: './remaining-tile.component.html',
  styleUrls: ['./remaining-tile.component.scss'],
  animations: [
    trigger('rotatedState', [
      state('0', style({transform: 'rotate(0deg)'})),
      state('90', style({transform: 'rotate(90deg)'})),
      state('180', style({transform: 'rotate(180deg)'})),
      state('270', style({transform: 'rotate(270deg)'})),
      transition('0 => 90', animate('200ms ease-out')),
      transition('90 => 180', animate('200ms ease-out')),
      transition('180 => 270', animate('200ms ease-out'))
    ])
  ]
})
export class RemainingTileComponent implements OnInit {
  @Input() remainingMazeCard: MazeCard;
  @Input() gameStompClient;
  @Output() orientationTurn = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.gameStompClient.subscribe('/game/remaining', res => {
        this.remainingMazeCard = JSON.parse(res.body);
    });
  }

  updateRemaining(mazeCard) {
    this.gameStompClient.send('/gameParty/updateRemaining', {}, JSON.stringify(mazeCard));
  }

  rotate() {
    if (this.remainingMazeCard.orientation.toString() === '0') {
      this.remainingMazeCard.orientation = 90;
      this.updateRemaining(this.remainingMazeCard);
    } else if (this.remainingMazeCard.orientation.toString() === '90') {
      this.remainingMazeCard.orientation = 180;
      this.updateRemaining(this.remainingMazeCard);
    } else if (this.remainingMazeCard.orientation.toString() === '180') {
      this.remainingMazeCard.orientation = 270;
      this.updateRemaining(this.remainingMazeCard);
    } else if (this.remainingMazeCard.orientation.toString() === '270') {
      this.remainingMazeCard.orientation = 0;
      this.updateRemaining(this.remainingMazeCard);
    }
    this.orientationTurn.emit(this.remainingMazeCard.orientation);
  }
}
