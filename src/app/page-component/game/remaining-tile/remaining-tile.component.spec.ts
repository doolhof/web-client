import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemainingTileComponent } from './remaining-tile.component';
import {AppModule} from '../../../app.module';

describe('RemainingTileComponent', () => {
  let component: RemainingTileComponent;
  let fixture: ComponentFixture<RemainingTileComponent>;

  beforeEach(() => TestBed.configureTestingModule({imports: [AppModule]}));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemainingTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
