import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-game-menu',
  templateUrl: './game-menu.component.html',
  styleUrls: ['./game-menu.component.scss']
})
export class GameMenuComponent implements OnInit {
  @Output() back = new EventEmitter();

  constructor(private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
  }

  onMainMenu() {
    this.router.navigateByUrl('/');
  }

  onBack() {
    this.back.emit();
  }
}
