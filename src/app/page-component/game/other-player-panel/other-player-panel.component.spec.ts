import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherPlayerPanelComponent } from './other-player-panel.component';

describe('OtherPlayerPanelComponent', () => {
  let component: OtherPlayerPanelComponent;
  let fixture: ComponentFixture<OtherPlayerPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherPlayerPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherPlayerPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
