import {Component, Input, OnInit} from '@angular/core';
import {Player, Treasure} from '../../../../model/game/game';

@Component({
  selector: 'app-other-player-panel',
  templateUrl: './other-player-panel.component.html',
  styleUrls: ['./other-player-panel.component.scss']
})
export class OtherPlayerPanelComponent implements OnInit {
  @Input() player: Player;

  constructor() {
  }

  ngOnInit() {
  }
}
