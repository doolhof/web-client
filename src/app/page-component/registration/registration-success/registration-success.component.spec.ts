import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationSuccessComponent } from './registration-success.component';
import {AppModule} from '../../../app.module';

describe('RegistrationSuccessComponent', () => {
  let component: RegistrationSuccessComponent;
  let fixture: ComponentFixture<RegistrationSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => TestBed.configureTestingModule({imports: [AppModule]}));

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
