import {Component, OnInit} from '@angular/core';
import {Token, User} from '../../../model/user/user';
import {UserService} from '../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorage} from '../../core/token.storage';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  user: User;
  email: string;
  password: string;
  passwordConf: string;
  invalidEmail: string;
  invalidPassword: string;
  username: string;
  token: Token;

  constructor(private  userService: UserService,
              private router: Router,
              private route: ActivatedRoute, private tokenstorage: TokenStorage) {
  }

  ngOnInit() {
  }

  readUsername(username): void {
    console.log('readusername');
    this.username = username;
  }

  readPassword(password): void {
    console.log('readpassword');
    this.password = password;
  }

  readPasswordConfirm(password): void {
    console.log('readpasswordconfirm');
    this.passwordConf = password;
    this.checkSamePassword();
  }

  checkSamePassword(): void {
    if (this.password === this.passwordConf) {
      this.invalidPassword = '';
    } else {
      this.invalidPassword = 'password is not the same';
    }
  }

  readMail(mail): void {
    console.log('readmail');
    this.email = mail;
    this.invalidEmail = '';
  }

  // TODO error handling voor username
  registrate(): void {
    console.log('registrate');
    if (this.invalidEmail === '' && this.invalidPassword === '' && this.username !== '') {
      try {
        this.userService.postSignUp(this.username, this.password, this.email).subscribe(res => {
          if (!(res instanceof HttpErrorResponse) && res !== null) {
            this.router.navigate(['register/success']);
          }
        });
      } catch (e) {
        console.log('error by registration: ' + e);
      }
    } else {
      console.log('wrong validation');
    }
  }
}
