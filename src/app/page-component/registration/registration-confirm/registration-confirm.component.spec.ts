import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationConfirmComponent } from './registration-confirm.component';
import {AppModule} from '../../../app.module';

describe('RegistrationConfirmComponent', () => {
  let component: RegistrationConfirmComponent;
  let fixture: ComponentFixture<RegistrationConfirmComponent>;

  beforeEach(() => TestBed.configureTestingModule({imports: [AppModule]}));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
