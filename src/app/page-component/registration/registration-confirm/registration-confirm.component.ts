import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../services/user.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-registration-confirm',
  templateUrl: './registration-confirm.component.html',
  styleUrls: ['./registration-confirm.component.scss']
})
export class RegistrationConfirmComponent implements OnInit {
  token: string;
  confirmed: boolean;

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService) {
    this.route.queryParams.subscribe(params => {
      this.token = params['token'];
    });
  }

  ngOnInit() {
    this.confirmed = false;
  }

  confirm() {
    console.log('confirm click' + this.token);
    this.userService.postConfirm(this.token).subscribe(res => {
        console.log(res);
        this.confirmed = !(res instanceof HttpErrorResponse);
      },
      error => {
        console.log('error');
      });
  }

  toSignIn() {
    this.router.navigate(['login']);
  }
}
