import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RegistrationComponent} from './registration.component';
import {AppModule} from '../../app.module';
import {MockComponent} from 'ng-mocks';
import {UserService} from '../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorage} from '../../core/token.storage';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {MainmenuComponent} from '../mainmenu/mainmenu.component';
import {HttpClientModule} from '@angular/common/http';


describe('RegistrationComponent', () => {
 /* const component = new RegistrationComponent(TestBed.get(UserService),
    TestBed.get(Router), TestBed.get(ActivatedRoute), TestBed.get(TokenStorage));
*/
  beforeEach(async (() => {
    TestBed.configureTestingModule({
      declarations: [MockComponent(RegistrationComponent)],
      imports: [RouterTestingModule, FormsModule, HttpClientModule],
      providers: [TokenStorage]
    })
      .compileComponents();
  }));


  it('checkSamePassword should not give invalidPassword', () => {
    const component = new RegistrationComponent(TestBed.get(UserService),
      TestBed.get(Router), TestBed.get(ActivatedRoute), TestBed.get(TokenStorage));
    component.passwordConf = 'password';
    component.password = 'password';
    component.checkSamePassword();
    expect(component.invalidPassword).toBe('');
  });
  /*beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
*/
  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
