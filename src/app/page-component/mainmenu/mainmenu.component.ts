import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../../model/user/user';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-mainmenu',
  templateUrl: './mainmenu.component.html',
  styleUrls: ['./mainmenu.component.scss']
})
export class MainmenuComponent implements OnInit {
  @Input() newGameValue = false;
  @Input() settingsValue = false;

  @Output() newGame = new EventEmitter();
  @Input() loadGames = new EventEmitter();
  @Output() loadGamesValue = false;
  loggedIn: boolean;
  user: User;
  /*   NOG TE VERWIJDEREN */
  constructor(private router: Router,
              private route: ActivatedRoute,
              private userService: UserService,
              private notifierService: NotifierService) {
    this.loggedIn = userService.isLoggedIn();
  }

  ngOnInit() {
    if (this.loggedIn) {
      this.user = this.userService.getLoggedInUser();
      this.notifierService.notify('default', 'Welcome, ' + this.user.username + '!');
    }
  }

  newGameClick() {
    if (this.loggedIn) {
      this.newGameValue = !this.newGameValue;
      this.newGame.emit(this.newGameValue);
      console.log('newgame: ' + this.newGameValue);
    } else {
      this.notifierService.notify('default', 'You have to sign in first.');
    }
  }

  profileClick() {
    this.router.navigate(['/profile'], {relativeTo: this.route});
  }


  changeToFalse(event) {
    this.newGameValue = event;
    this.loadGamesValue = event;
  }

  onLoadGames() {
    if (this.loggedIn) {
      this.loadGamesValue = !this.loadGamesValue;
      this.loadGames.emit(this.loadGamesValue);
      console.log('loadgames: ' + this.loadGamesValue);
    } else {
      this.notifierService.notify('default', 'You have to sign in first.');
    }
  }

  onFriends() {
    if (this.loggedIn) {
      this.router.navigate(['/friends']);
    } else {
      this.notifierService.notify('default', 'You have to sign in first.');
    }
  }

  onLogin() {
    this.router.navigateByUrl('/login');
  }

  onSignout() {
    this.userService.logout();
    this.loggedIn = false;
    this.notifierService.notify('default', 'You have been signed out.');
  }
}
