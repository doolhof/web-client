import {LobbyService} from '../../../services/lobby.service';
import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserService} from '../../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorage} from '../../../core/token.storage';
import {GameSettings, Lobby, User} from '../../../../model/user/user';

@Component({
  selector: 'app-gamesettings',
  templateUrl: './gamesettings.component.html',
  styleUrls: ['./gamesettings.component.scss']
})
export class GamesettingsComponent implements OnInit {
  data: { gameSettingId: number, amountPlayer: number, secondsTurn: number };
  currentUser: User = JSON.parse(window.localStorage.getItem('currentUser'));
  user: User;
  @Output() backToMenu = new EventEmitter();


  constructor(private userService: UserService,
              private router: Router,
              private route: ActivatedRoute, private tokenstorage: TokenStorage,
              private lobbyService: LobbyService) {
    this.data = {
      gameSettingId: 1,
      amountPlayer: 2,
      secondsTurn: 60
    };
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.data.secondsTurn && this.data.amountPlayer) {
      console.log(this.data.secondsTurn);
      this.lobbyService.postCreateLobby(this.currentUser.userId, this.data.secondsTurn, this.data.amountPlayer).subscribe(res => {
        this.router.navigate(['/lobby', res.lobbyId.toString()], {relativeTo: this.route});
      });
    }
  }

  toMenu() {
    this.backToMenu.emit(false);
    console.log('go to menu');
  }

}
