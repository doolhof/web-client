import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LobbyService} from '../../../services/lobby.service';
import {Lobby, User} from '../../../../model/user/user';
import {GameService} from '../../../services/game.service';

@Component({
  selector: 'app-unfinished-games',
  templateUrl: './unfinished-games.component.html',
  styleUrls: ['./unfinished-games.component.scss']
})
export class UnfinishedGamesComponent implements OnInit {
  @Output() backToMenu = new EventEmitter();
  selectedLobby: Lobby;
  inlogUser: User = JSON.parse(window.localStorage.getItem('currentUser'));
  lobbies: Lobby[];

  constructor(private lobbyService: LobbyService, private router: Router,
              private route: ActivatedRoute, private _gameService: GameService) {
  }

  ngOnInit() {
    this.lobbyService.getLobbiesUser(this.inlogUser.username).subscribe(res => {
      this.lobbies = res;
      console.log(res);
    });
  }
  toMenu() {
    this.backToMenu.emit(false);
    console.log('go to menu');
  }

  loadLobby() {
    this.router.navigate(['/lobby/' + this.selectedLobby.lobbyId], {relativeTo: this.route});
  }

  onSelect(lobby: Lobby): void {
    this.selectedLobby = lobby;
  }

  replayGame() {
    this._gameService.getReplayGame(this.selectedLobby.game.gameId).subscribe(res => {
      this.router.navigate(['/game/replay/' + res.gameId], {relativeTo: this.route});
    });
  }
}
