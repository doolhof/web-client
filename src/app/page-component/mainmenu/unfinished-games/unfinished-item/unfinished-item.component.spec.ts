import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnfinishedItemComponent } from './unfinished-item.component';
import {AppModule} from '../../../../app.module';

describe('UnfinishedItemComponent', () => {
  let component: UnfinishedItemComponent;
  let fixture: ComponentFixture<UnfinishedItemComponent>;

  beforeEach(() => TestBed.configureTestingModule({imports: [AppModule]}));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnfinishedItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnfinishedItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

 /* it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
