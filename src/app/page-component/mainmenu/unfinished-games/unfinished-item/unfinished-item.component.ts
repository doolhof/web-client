import {Component, Input, OnInit} from '@angular/core';
import {Lobby, User} from '../../../../../model/user/user';
import {GameService} from '../../../../services/game.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-unfinished-item',
  templateUrl: './unfinished-item.component.html',
  styleUrls: ['./unfinished-item.component.scss']
})
export class UnfinishedItemComponent implements OnInit {
  @Input() public lobby: Lobby;
  @Input() public selected: boolean;
  inlogUser: User;

  constructor(private _gameService: GameService, private router: Router,
              private route: ActivatedRoute) {
  this.inlogUser = JSON.parse(window.localStorage.getItem('currentUser'));
  }


  ngOnInit() {
  }
  replayGame() {
    this._gameService.getReplayGame(this.lobby.game.gameId).subscribe(res => {
      this.router.navigate(['/game/replay/' + res.gameId], {relativeTo: this.route});
    });
  }
}

