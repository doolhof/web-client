import {async, TestBed} from '@angular/core/testing';

import {UnfinishedGamesComponent} from './unfinished-games.component';
import {UnfinishedItemComponent} from './unfinished-item/unfinished-item.component';
import {RouterTestingModule} from '@angular/router/testing';
import {LobbyService} from '../../../services/lobby.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {Game} from '../../../../model/game/game';

describe('UnfinishedGamesComponent', () => {
  const user = {
    userId: 1, username: 'root',
    friends: [],
    email: 'root@root.com',
    encryptedPassword: 'root',
    lobbies: null,
    userStatistic: null,
    userSetting: null,
    roles: null
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UnfinishedGamesComponent, UnfinishedItemComponent],
      imports: [RouterTestingModule, HttpClientModule ]
    })
      .compileComponents();
  }));
  it('should emit backToMenu event', () => {
    const service: LobbyService = TestBed.get(LobbyService);
    const router: Router = TestBed.get(Router);
    const route: ActivatedRoute = TestBed.get(ActivatedRoute);
    const component = new UnfinishedGamesComponent( service, router, route);
    component.backToMenu.subscribe(g => { expect(g).toEqual({backToMenu: false});
    });
  });
  it('should change the selectedlobby', () => {
    const service: LobbyService = TestBed.get(LobbyService);
    const router: Router = TestBed.get(Router);
    const route: ActivatedRoute = TestBed.get(ActivatedRoute);
    const component = new UnfinishedGamesComponent( service, router, route);
    const lobby = {
      lobbyId: 1, gameSettings:
        {gameSettingId: 1, amountOfUsers: 2, secondsPerTurn: 50}, listUsers: [user], host: user, game: null
    };
    lobby.listUsers = [user];
    component.lobbies = [lobby];
    component.onSelect(lobby);
    expect(component.lobbies).toEqual([lobby]);
  });


});
