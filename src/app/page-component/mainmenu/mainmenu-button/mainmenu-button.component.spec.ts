import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainmenuButtonComponent } from './mainmenu-button.component';

describe('MainmenuButtonComponent', () => {
  let component: MainmenuButtonComponent;
  let fixture: ComponentFixture<MainmenuButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainmenuButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainmenuButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
