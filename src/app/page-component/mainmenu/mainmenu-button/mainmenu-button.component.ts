import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-mainmenu-button',
  templateUrl: './mainmenu-button.component.html',
  styleUrls: ['./mainmenu-button.component.scss']
})
export class MainmenuButtonComponent implements OnInit {

  @Input() label: string;

  constructor() {
  }

  ngOnInit() {
  }
}
