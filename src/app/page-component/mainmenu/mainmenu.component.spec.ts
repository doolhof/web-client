import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MainmenuComponent} from './mainmenu.component';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {MockComponent} from 'ng-mocks';
import {HttpClientModule} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {NotifierService} from 'angular-notifier';


describe('MainmenuComponent', () => {
  // const component = new MainmenuComponent();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MockComponent(MainmenuComponent)],
      imports: [RouterTestingModule, FormsModule, HttpClientModule],
      providers: [UserService, NotifierService]
    })
      .compileComponents();
  }));
  it('should make newGameValue true', () => {
    const component = new MainmenuComponent(TestBed.get(Router), TestBed.get(ActivatedRoute), TestBed.get(UserService), TestBed.get(NotifierService));
    component.newGameClick();
    expect(component.newGameValue).toBe(true);
  });
  it('newGameValue and loadGamesValue should be false', () => {
    const component = new MainmenuComponent(TestBed.get(Router), TestBed.get(ActivatedRoute), TestBed.get(UserService), TestBed.get(NotifierService));
    component.newGameClick();
    component.changeToFalse(false);
    expect(component.newGameValue).toBe(false);
    expect(component.loadGamesValue).toBe(false);
  });

  it('should create', () => {
    const component = new MainmenuComponent(TestBed.get(Router), TestBed.get(ActivatedRoute), TestBed.get(UserService), TestBed.get(NotifierService));
    expect(component).toBeTruthy();
  });
});

