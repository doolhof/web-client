import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxRememberComponent } from './checkbox-remember.component';

describe('CheckboxRememberComponent', () => {
  let component: CheckboxRememberComponent;
  let fixture: ComponentFixture<CheckboxRememberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckboxRememberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxRememberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
