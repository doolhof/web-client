import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../../../model/user/user';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-inputbox-login',
  templateUrl: './inputbox-login.component.html',
  styleUrls: ['./inputbox-login.component.scss']
})
export class InputboxLoginComponent implements OnInit {

  @Input() placeholderInput: string;
  @Output() signInfo = new EventEmitter<User>();
  usernameFormControl = new FormControl('', [
    Validators.required,
  ]);


  constructor() {
  }

  ngOnInit() {
  }

  sendSignInfo(info) {
    this.signInfo.emit(info);
  }
}
