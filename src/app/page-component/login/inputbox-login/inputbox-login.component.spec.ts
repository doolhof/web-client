import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputboxLoginComponent } from './inputbox-login.component';
import {AppModule} from '../../../app.module';

describe('InputboxLoginComponent', () => {
  let component: InputboxLoginComponent;
  let fixture: ComponentFixture<InputboxLoginComponent>;

  beforeEach(() => TestBed.configureTestingModule({imports: [AppModule]}));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputboxLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
