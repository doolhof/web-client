import {Component, NgZone, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {FbInfo, User} from '../../../model/user/user';
import {ActivatedRoute, Router} from '@angular/router';
import {TokenStorage} from '../../core/token.storage';
import {HttpErrorResponse} from '@angular/common/http';
import {createNgZone} from '@angular/platform-browser/testing/src/browser_util';
import {NotifierService} from 'angular-notifier';

declare const FB: any;


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  fbInfo: FbInfo;
  user: User;
  email: string;
  password: string;
  invalidEmail: string;
  invalidPassword: string;
  authenticated: Boolean = false;
  username: string;
  constructor(private userService: UserService,
              private router: Router,
              private route: ActivatedRoute,
              private tokenstorage: TokenStorage,
              private ngZone: NgZone,
              private notifierService: NotifierService) {
  }
  ngOnInit() {
    this.tokenstorage.signOut();
    (window as any).fbAsyncInit = function() {
      FB.init({
        appId      : '1072072529583847',
        cookie     : true,
        xfbml      : true,
        version    : 'v3.1'
      });
      FB.AppEvents.logPageView();
    };

    (function(d, s, id) {
      let js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return; }
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }
  signInWithEmail(): void {
    this.userService.postLogin(this.username, this.password).subscribe(res => {
      if (res.token !== null) {
        console.log(res);
        this.authenticated = true;
        console.log(this.authenticated);
        this.tokenstorage.saveToken(res.token);
      }
      if (!this.authenticated) {
        this.notifierService.notify('default', 'Welcome, ' + this.user.username + '!');
      }
      if (this.authenticated) {
        console.log('get user');
        this.userService.getUserByUsername(this.username).subscribe(res1 => {
          if (!(res1 instanceof HttpErrorResponse) && res1 !== null) {
            console.log(JSON.stringify(res1));
            window.localStorage.setItem('currentUser', JSON.stringify(res1));
            this.redirectToMainMenu();
          }
        });
      }
    }, error => {
      this.invalidEmail = 'wrong username';
      this.invalidPassword = '';
    });
  }
  signinWithFacebook(): void {
    console.log('submit login to facebook');
    FB.login((response) => {
      console.log('submitLogin', response);
      if (response.authResponse) {
        // succesfull login
        console.log(response.authResponse.accessToken);
        this.me(response.authResponse.userID, response.authResponse.accessToken);
      } else {
        console.log('User login failed');
      }
    });
  }

  me(userId, accessToken) {
    FB.api(
      '/' + userId + '?fields=id,name,first_name,email,gender',
      (result) => {
        console.log('result===', result);
        this.fbInfo = new FbInfo(result);
        this.userService.createAccountWithFb(this.fbInfo).subscribe(res => {
          this.username = res.username;
          this.password = res.encryptedPassword;
          this.signInWithEmail();
        });
      });
  }

  redirectToMainMenu(): void {
    this.ngZone.run(() => this.router.navigate(['/mainmenu'], {relativeTo: this.route}));
  }

  /*
    readEmail(email): void {
      this.email = email;
    }
    */
  readUsername(username): void {
    this.username = username;
  }

  readPassword(password): void {
    this.password = password;
  }

  passwordValidator(): boolean {
    return this.user.encryptedPassword === this.password && this.user.encryptedPassword.length !== 0;
  }

  register(): void {
    console.log('register');
    this.router.navigate(['/register']);
  }

}
