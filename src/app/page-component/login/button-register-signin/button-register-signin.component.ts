import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-button-register-signin',
  templateUrl: './button-register-signin.component.html',
  styleUrls: ['./button-register-signin.component.scss']
})
export class ButtonRegisterSigninComponent implements OnInit {

@Input() label: string;
@Input() center: boolean;
@Output() btnClick = new EventEmitter();




  constructor() { }

  ngOnInit() {
  }

}
