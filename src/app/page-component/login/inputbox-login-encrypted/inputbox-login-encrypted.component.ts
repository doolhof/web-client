import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../../../model/user/user';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-inputbox-login-encrypted',
  templateUrl: './inputbox-login-encrypted.component.html',
  styleUrls: ['./inputbox-login-encrypted.component.scss']
})
export class InputboxLoginEncryptedComponent implements OnInit {
  placeholderInput: string;
  @Output() signInfo = new EventEmitter<User>();
  passwordFormControl = new FormControl('', [
    Validators.required,
  ]);

  constructor() {
  }

  ngOnInit() {
  }

  sendSignInfo(info) {
    this.signInfo.emit(info);
  }

}
