import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputboxLoginEncryptedComponent } from './inputbox-login-encrypted.component';
import {AppModule} from '../../../app.module';

describe('InputboxLoginEncryptedComponent', () => {
  let component: InputboxLoginEncryptedComponent;
  let fixture: ComponentFixture<InputboxLoginEncryptedComponent>;

  beforeEach(() => TestBed.configureTestingModule({imports: [AppModule]}));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputboxLoginEncryptedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
