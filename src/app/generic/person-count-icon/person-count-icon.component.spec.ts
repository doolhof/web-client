import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonCountIconComponent } from './person-count-icon.component';

describe('PersonCountIconComponent', () => {
  let component: PersonCountIconComponent;
  let fixture: ComponentFixture<PersonCountIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonCountIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonCountIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
