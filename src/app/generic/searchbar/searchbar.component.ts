import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent implements OnInit {
  @Output() selectableItemsEmitter = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  search(event): void {
    this.selectableItemsEmitter.emit(event);
  }
}
