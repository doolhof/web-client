import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatMessageContentAreaComponent } from './chat-message-content-area.component';

describe('ChatMessageContentAreaComponent', () => {
  let component: ChatMessageContentAreaComponent;
  let fixture: ComponentFixture<ChatMessageContentAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatMessageContentAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatMessageContentAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
