import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';


@Component({
  selector: 'app-chat-inputbox',
  templateUrl: './chat-inputbox.component.html',
  styleUrls: ['./chat-inputbox.component.scss']
})
export class ChatInputboxComponent implements OnInit {

  @ViewChild('textArea') textInput: ElementRef;
  @Output() messageEmitter: EventEmitter<string> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  sendMessage($event) {
    this.messageEmitter.emit($event);
    this.textInput.nativeElement.value = '';
  }

}
