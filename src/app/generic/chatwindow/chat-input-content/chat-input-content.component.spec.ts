import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatInputContentComponent } from './chat-input-content.component';

describe('ChatInputContentComponent', () => {
  let component: ChatInputContentComponent;
  let fixture: ComponentFixture<ChatInputContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatInputContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatInputContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
