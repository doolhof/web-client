import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-chat-input-content',
  templateUrl: './chat-input-content.component.html',
  styleUrls: ['./chat-input-content.component.scss']
})
export class ChatInputContentComponent implements OnInit {

  @Output() messageEmitter: EventEmitter<string> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  sendMessage($event) {
    this.messageEmitter.emit($event);
  }

}
