import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-label-person-count',
  templateUrl: './label-person-count.component.html',
  styleUrls: ['./label-person-count.component.scss']
})
export class LabelPersonCountComponent implements OnInit {
  @Input() online: number;
  @Input() total: number;

  constructor() {
  }

  ngOnInit() {
  }

}
