import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabelPersonCountComponent } from './label-person-count.component';

describe('LabelPersonCountComponent', () => {
  let component: LabelPersonCountComponent;
  let fixture: ComponentFixture<LabelPersonCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabelPersonCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabelPersonCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
