import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClickableItemComponent } from './clickable-item.component';

describe('ClickableItemComponent', () => {
  let component: ClickableItemComponent;
  let fixture: ComponentFixture<ClickableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClickableItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClickableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*it('should create', () => {
    expect(component).toBeTruthy();
  });*/
});
