import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DefaultButtonComponent} from './generic/default-button/default-button.component';
import {ButtonInviteComponent} from './page-component/lobby/invites/list-area/list/button-invite/button-invite.component';
import {ChatwindowComponent} from './generic/chatwindow/chatwindow.component';
import {ChatInputContentComponent} from './generic/chatwindow/chat-input-content/chat-input-content.component';
import {ChatInputboxComponent} from './generic/chatwindow/chat-input-content/chat-inputbox/chat-inputbox.component';
import {ChatMessageContentAreaComponent} from './generic/chatwindow/chat-message-content-area/chat-message-content-area.component';
import {ClickableItemComponent} from './generic/clickable-item/clickable-item.component';
import {LabelPersonCountComponent} from './generic/label-person-count/label-person-count.component';
import {NextTurnButtonComponent} from './generic/next-turn-button/next-turn-button.component';
import {PersonCountIconComponent} from './generic/person-count-icon/person-count-icon.component';
import {ProfilePictureComponent} from './generic/profile-picture/profile-picture.component';
import {SearchbarComponent} from './generic/searchbar/searchbar.component';
import {LobbyComponent} from './page-component/lobby/lobby.component';
import {GameComponent} from './page-component/game/game.component';
import {MainmenuComponent} from './page-component/mainmenu/mainmenu.component';
import {ProfileComponent} from './page-component/profile/profile.component';
import {RegistrationComponent} from './page-component/registration/registration.component';
import {LeaderboardComponent} from './page-component/game/leaderboard/leaderboard.component';
import {TileComponent} from './page-component/game/gameboard/tile/tile.component';
import {TreasureComponent} from './page-component/game/gameboard/treasure/treasure.component';
import {GamepartyComponent} from './page-component/lobby/gameparty/gameparty.component';
import {HostPartySettingComponent} from './page-component/lobby/gameparty/host-party-setting/host-party-setting.component';
import {OtherPlayersComponent} from './page-component/lobby/gameparty/other-players/other-players.component';
import {GamesettingsComponent} from './page-component/mainmenu/gamesettings/gamesettings.component';
import {InvitesComponent} from './page-component/lobby/invites/invites.component';
import {ListItemComponent} from './page-component/lobby/invites/list-area/list/list-item/list-item.component';
import {ListComponent} from './page-component/lobby/invites/list-area/list/list.component';
import {ListAreaComponent} from './page-component/lobby/invites/list-area/list-area.component';
import {LoginComponent} from './page-component/login/login.component';
import {ButtonRegisterSigninComponent} from './page-component/login/button-register-signin/button-register-signin.component';
import {CheckboxRememberComponent} from './page-component/login/checkbox-remember/checkbox-remember.component';
import {InputboxLoginComponent} from './page-component/login/inputbox-login/inputbox-login.component';
import {MainmenuButtonComponent} from './page-component/mainmenu/mainmenu-button/mainmenu-button.component';
import {GameboardComponent} from './page-component/game/gameboard/gameboard.component';
import {RouterModule, Routes} from '@angular/router';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatBadgeModule} from '@angular/material/badge';


import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthGuard} from './core/auth.guard';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TokenStorage} from './core/token.storage';
import {UserService} from './services/user.service';
import {GameService} from './services/game.service';
import {Interceptor} from './core/interceptor';
import {ErrorsHandler} from './core/errorHandler';
import {InputboxLoginEncryptedComponent} from './page-component/login/inputbox-login-encrypted/inputbox-login-encrypted.component';
import {RemainingTileComponent} from './page-component/game/remaining-tile/remaining-tile.component';
import {OtherPlayerItemComponent} from './page-component/lobby/gameparty/other-players/other-player-item/other-player-item.component';
import {RegistrationSuccessComponent} from './page-component/registration/registration-success/registration-success.component';

import {PawnComponent} from './page-component/game/gameboard/pawn/pawn.component';
import {PlayerService} from './services/player.service';
import {MatIconModule} from '@angular/material/icon';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RegistrationConfirmComponent} from './page-component/registration/registration-confirm/registration-confirm.component';
import {HostProfileComponent} from './page-component/lobby/gameparty/host-profile/host-profile.component';
import {UnfinishedGamesComponent} from './page-component/mainmenu/unfinished-games/unfinished-games.component';
import {UnfinishedItemComponent} from './page-component/mainmenu/unfinished-games/unfinished-item/unfinished-item.component';
import {LobbyService} from './services/lobby.service';
import {UserPlayerPanelComponent} from './page-component/game/user-player-panel/user-player-panel.component';
import {OtherPlayerPanelComponent} from './page-component/game/other-player-panel/other-player-panel.component';
import {LottieAnimationViewModule} from 'lottie-angular2';
import {TurnStatusComponent} from './page-component/game/turn-status/turn-status.component';
import {GameMenuComponent} from './page-component/game/game-menu/game-menu.component';
import {MatDialogModule} from '@angular/material/dialog';
import {EditPasswordComponent} from './page-component/profile/edit-password/edit-password.component';
import {FriendStatisticsComponent} from './page-component/friends/friend-statistics/friend-statistics.component';
import {DeleteAccountComponent} from './page-component/profile/delete-account/delete-account.component';
import {FriendsComponent} from './page-component/friends/friends.component';
import {FriendItemComponent} from './page-component/friends/friend-item/friend-item.component';
import {AddFriendMenuComponent} from './page-component/friends/add-friend-menu/add-friend-menu.component';
import {MatchItemComponent} from './page-component/friends/add-friend-menu/match-item/match-item.component';
import {NotifierModule} from 'angular-notifier';
import {ErrorPageComponent} from './page-component/error-page/error-page.component';
import {GameChatComponent} from './page-component/game/game-chat/game-chat.component';
import {GameChatInputContentComponent} from './page-component/game/game-chat/game-chat-input-content/game-chat-input-content.component';
import {GameChatMessageContentAreaComponent} from './page-component/game/game-chat/game-chat-message-content-area/game-chat-message-content-area.component';
import {GameChatInputboxComponent} from './page-component/game/game-chat/game-chat-input-content/game-chat-inputbox/game-chat-inputbox.component';
import {NotificationComponent} from './page-component/notification/notification.component';
import {NotificationListComponent} from './page-component/notification/notification-list/notification-list.component';
import {NotificationItemComponent} from './page-component/notification/notification-list/notification-item/notification-item.component';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireModule} from '@angular/fire';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireMessagingModule} from '@angular/fire/messaging';
import {environment} from '../environments/environment';

const appRoutes: Routes = [
  {path: 'leaderBoard', component: LeaderboardComponent, canActivate: [AuthGuard]},
  {path: 'game/:id', component: GameComponent, canActivate: [AuthGuard]},
  {path: 'game/replay/:id', component: GameComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'lobby', component: LobbyComponent, canActivate: [AuthGuard]},
  {path: 'lobby/:id', component: LobbyComponent, canActivate: [AuthGuard]},
  {path: 'gameSetting', component: GamesettingsComponent, canActivate: [AuthGuard]},
  {path: 'profile', component: ProfileComponent},
  {path: 'register', component: RegistrationComponent},
  {path: 'register/success', component: RegistrationSuccessComponent},
  {path: 'user/:id', component: FriendStatisticsComponent, canActivate: [AuthGuard]},
  {path: 'register/confirm', component: RegistrationConfirmComponent},
  {path: 'friends', component: FriendsComponent, canActivate: [AuthGuard]},
  {path: 'error', component: ErrorPageComponent},
  {path: '**', component: MainmenuComponent}
];
const config = {
  apiKey: 'AIzaSyD2hHT_qHQe6hfbglnFnlbkq9dvqYZ9iOE',
  authDomain: 'mazetrix-c09ac.firebaseapp.com',
  databaseURL: 'https://mazetrix-c09ac.firebaseio.com/',
  projectId: 'mazetrix-c09ac',
  storageBucket: 'mazetrix-c09ac.appspot.com',
  messagingSenderId: '250908293848'
};

@NgModule({
  declarations: [
    AppComponent,
    DefaultButtonComponent,
    ButtonInviteComponent,
    ChatwindowComponent,
    ChatInputContentComponent,
    ChatInputboxComponent,
    ChatMessageContentAreaComponent,
    ClickableItemComponent,
    LabelPersonCountComponent,
    NextTurnButtonComponent,
    PersonCountIconComponent,
    ProfilePictureComponent,
    SearchbarComponent,
    LobbyComponent,
    GameComponent,
    MainmenuComponent,
    ProfileComponent,
    RegistrationComponent,
    LeaderboardComponent,
    TileComponent,
    TreasureComponent,
    GamepartyComponent,
    HostPartySettingComponent,
    OtherPlayersComponent,
    GamesettingsComponent,
    InvitesComponent,
    ListItemComponent,
    ListComponent,
    ListAreaComponent,
    LoginComponent,
    ButtonRegisterSigninComponent,
    CheckboxRememberComponent,
    InputboxLoginComponent,
    MainmenuButtonComponent,
    GameboardComponent,
    InputboxLoginEncryptedComponent,
    RemainingTileComponent,
    OtherPlayerItemComponent,
    RegistrationSuccessComponent,
    PawnComponent,
    RegistrationConfirmComponent,
    HostProfileComponent,
    UnfinishedGamesComponent,
    UnfinishedItemComponent,
    UserPlayerPanelComponent,
    OtherPlayerPanelComponent,
    TurnStatusComponent,
    GameMenuComponent,
    EditPasswordComponent,
    DeleteAccountComponent,
    FriendsComponent,
    FriendItemComponent,
    AddFriendMenuComponent,
    MatchItemComponent,
    ErrorPageComponent,
    FriendStatisticsComponent,
    GameChatComponent,
    GameChatInputContentComponent,
    ChatInputContentComponent,
    ChatInputboxComponent,
    ChatMessageContentAreaComponent,
    NotificationComponent,
    NotificationListComponent,
    NotificationItemComponent,
    GameChatMessageContentAreaComponent,
    GameChatInputboxComponent
  ],
  imports: [
    BrowserModule,
    LottieAnimationViewModule.forRoot(),
    BrowserAnimationsModule,
    AppRoutingModule,
    MatBadgeModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatSlideToggleModule,
    MatDialogModule,
    RouterModule.forRoot(
      appRoutes, {
        enableTracing: false
      }
    ),
    BrowserAnimationsModule,
    NotifierModule.withConfig({
      position: {
        horizontal: {
          position: 'right',
          distance: 12
        },
        vertical: {
          position: 'bottom',
          distance: 12,
          gap: 10
        }
      },
      behaviour: {
        autoHide: 4000,
        onClick: 'hide',
        onMouseover: 'pauseAutoHide',
        showDismissButton: true,
        stacking: 5
      },
      animations: {
        enabled: true,
        show: {
          preset: 'slide',
          speed: 300,
          easing: 'ease'
        },
        hide: {
          preset: 'fade',
          speed: 300,
          easing: 'ease',
          offset: 50
        },
        shift: {
          speed: 300,
          easing: 'ease'
        },
        overlap: 150
      }
    }),
    AngularFireDatabaseModule,      // for db
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
  ],
  exports: [
    UnfinishedItemComponent,
    UnfinishedGamesComponent
  ],
  providers: [TokenStorage, HttpClientModule, LobbyService, GameService, UserService, PlayerService,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }, {
      provide: ErrorHandler,
      useClass: ErrorsHandler,
    }],

  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
