import {GameSocketMessage, Player} from '../model/game/game';

export function getNextPlayer(playerList: Player[], playerTurn: Player) {
  for (let _i = 0; _i < playerList.length; _i++) {
    if (playerList[_i].playerId === playerTurn.playerId) {
      playerList[_i].turn = false;
      if (playerList.length - 1 === _i) {
        _i = 0;
      } else {
        _i += 1;
      }
      return _i;
    }
  }
}

export function getGame(gameId, userId, gameStompClient) {
  const socketMessage = new GameSocketMessage(gameId.toString(), userId.toString());
  gameStompClient.send('/gameParty/getGame.joinGame', {}, JSON.stringify(socketMessage));
}

