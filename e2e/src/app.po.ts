import {browser, by, element, protractor} from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/login');
  }

  setUsername() {
    element(by.css('app-inputbox-login')).element(by.name('inputUsername')).clear().then(function () {
      element(by.css('app-inputbox-login')).element(by.name('inputUsername')).sendKeys('root');
    });
    return element(by.css('app-inputbox-login')).element(by.name('inputUsername')).getAttribute('value');
  }

  setPassword() {
      element(by.css('app-inputbox-login-encrypted')).element(by.name('inputPassword')).clear().then(function () {
      element(by.css('app-inputbox-login-encrypted')).element(by.name('inputPassword')).sendKeys('root');
    });
    return element(by.css('app-inputbox-login-encrypted')).element(by.name('inputPassword')).getAttribute('value');
  }

  pushLogin() {
    return element(by.css('app-button-register-signin')).element(by.css('button'));
  }
  pushArrowDown() {
    return element(by.id('arrowDown'));
  }
}
